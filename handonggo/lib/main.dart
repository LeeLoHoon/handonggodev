
import 'package:cool_ui/cool_ui.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:handong_go/blocs/authentication_bloc/bloc.dart';
import 'package:handong_go/blocs/setting_bloc/bloc.dart';
import 'package:handong_go/pages/bottom_navigation.dart';
import 'package:handong_go/pages/i18n/messages.dart';
import 'package:handong_go/pages/login/login_screen.dart';
import 'package:handong_go/pages/splash_screen.dart';
import 'package:handong_go/utils/LocaleUtil.dart';
import 'package:handong_go/utils/user_repository.dart';

import 'pages/i18n/app_localizations.dart';

import 'blocs/authentication_bloc/authentication_bloc.dart';
import 'blocs/authentication_bloc/authentication_event.dart';
import 'blocs/authentication_bloc/authentication_state.dart';

main() {
  NumberKeyboard.register();
  runApp(App());
}

class App extends StatefulWidget {
  State<App> createState() => _AppState();
}



class _AppState extends State<App> {
  final UserRepository _userRepository = UserRepository();
  AuthenticationBloc _authenticationBloc;
  SettingBloc _settingBloc;
  final msg = Messages();

  @override
  void initState() {
    super.initState();

    _authenticationBloc = AuthenticationBloc(userRepository: _userRepository);
    _authenticationBloc.add(AppStarted());
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider.value(
      value: _authenticationBloc,
      child: MaterialApp(
        supportedLocales: [
          const Locale('ko', 'KO'),
          const Locale('en', 'EN'),
          const Locale('viet','VIET')
        ],
        localizationsDelegates: [
          AppLocalizationDelegate(),
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
        ],


        //theme: ThemeData(fontFamily: 'hgoFont'),
        title: "HandongGo",
        debugShowCheckedModeBanner: false,
        home: BlocBuilder(
          bloc: _authenticationBloc,
          builder: (BuildContext context, AuthenticationState state) {
            if (state.uninitialized) {
              return SplashScreen();
            } else if (state.unAuthenticated) {
              return LoginScreen(userRepository: _userRepository);
            }
            else if (state.authenticated) {
              return MultiBlocProvider(
                providers: [
                  BlocProvider<SettingBloc>(
                    create: (BuildContext context) => SettingBloc(),
                  ),

                ],

                child: BottomNaviBar(),
              );


              //return BottomNaviBar();
            }
            return SplashScreen();
          },
        ),
      ),
    );
  }
}

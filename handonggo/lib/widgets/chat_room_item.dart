import 'package:auto_size_text/auto_size_text.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:handong_go/pages/chatting/chatting_room.dart';
import 'package:handong_go/utils/LocaleUtil.dart';
import 'package:handong_go/utils/user_util.dart';

class ChatRoomItem extends StatelessWidget {
  final DocumentSnapshot ds;
  final AsyncSnapshot memberSnapshot;
  final BuildContext context;
  final Function refresh;
  final String _locale = LocaleUtil.getLocale();

  ChatRoomItem(
      {Key key, this.ds, this.memberSnapshot, this.context, this.refresh})
      : super(key: key);

  bool isJoined() => ds[UserUtil.getUser().uid] == true;
  bool isMax() => memberSnapshot.data.documents.length >= ds.data['peopleMax'];

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        isJoined()
            ? goToRoom()
            : !isMax() ? showJoinChatRoomDialog() : showFullDialog();
      },
      child: Container(
        width: MediaQuery.of(context).size.width * 0.9,
        child: Column(
          children: <Widget>[
            Row(
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                Padding(
                  padding:
                  const EdgeInsets.symmetric(horizontal: 10, vertical: 15),
                  child: CircleAvatar(
                    radius: 25,
                    backgroundImage:
                    CachedNetworkImageProvider(ds['chatRoomImage']
                    ),
                  ),
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.fromLTRB(0, 0, 5, 0),
                          child: Text(
                            _locale == "ko" ? ds['koName'] : ds['enName'],
                            style: TextStyle(
                                fontWeight: FontWeight.w600, fontSize: 17),
                          ),
                        ),
                        Text(
                          '(${memberSnapshot.data.documents.length}/${ds['peopleMax']})',
                          style: TextStyle(color: Colors.grey, fontSize: 15),
                        )
                      ],
                    ),
                    SizedBox(height: 3),
                    Container(
                      width: MediaQuery.of(context).size.width * .65,
                      child: AutoSizeText(
                        _locale == "ko" ? ds['koDesc'] : ds['enDesc'],
                        maxLines: 1,
                        minFontSize: 15,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 15,
                            fontWeight: FontWeight.w500),
                      ),
                    ),
                  ],
                ),
              ],
            ),
            Divider(
              height: 1,
            ),
          ],
        ),
      ),
    );
  }

  void showJoinChatRoomDialog() {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
              title: Text("ddd"),
              content: Text(''),
              actions: <Widget>[
                OutlineButton(
                  child: Text(
                    "no",
                  ),
                  borderSide: BorderSide(style: BorderStyle.none),
                  onPressed: () {
                    Navigator.pop(context);
                  },
                ),
                OutlineButton(
                  child: Text("yes"),
                  onPressed: () {
                    Navigator.pop(context);
                    joinRoom();
                  },
                  borderSide: BorderSide(style: BorderStyle.none),
                ),
              ]);
        });
  }

  void showFullDialog() {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
              title: Text("초과"),
              content: Text("못들어가"),
              actions: <Widget>[
                OutlineButton(
                  child: Text(
                    "예스",
                  ),
                  borderSide: BorderSide(style: BorderStyle.none),
                  onPressed: () {
                    Navigator.pop(context);
                  },
                ),
              ]);
        });
  }

  void joinRoom() async {
    try {
      WriteBatch batch = Firestore.instance.batch();
      batch.setData(
          Firestore.instance
              .collection('RoomMember')
              .document(UserUtil.getUser().uid + ds.data['roomNo']),
          {'uid': UserUtil.getUser().uid, 'roomNo': ds.data['roomNo']},
          merge: true);
      batch.setData(
          Firestore.instance
              .collection('ChatRooms')
              .document(ds.data['roomNo']),
          {'${UserUtil.getUser().uid}': true},
          merge: true);
      await batch.commit();
      refresh();
      goToRoom();
    } catch (e) {
      print(e.toString());
    }
  }

  void goToRoom() {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ChatRoomPage(
              roomId: ds.data['roomNo'],
              roomNm: ds.data['roomNm'],
              peopleCnt: memberSnapshot.data.documents.length,
              peopleMax: ds.data['peopleMax'],
              account: ds.data['account'],
            ))).then((void v) => refresh != null ? refresh() : () {});
  }
}

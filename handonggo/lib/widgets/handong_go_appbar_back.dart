import 'package:flutter/material.dart';
import 'package:handong_go/utils/colors.dart';

class HgoBackAppBar extends AppBar {
  final Color backgroundColor = mainColor;
  final double elevation = 0.0;
  final bool centerTitle = true;
  final actions;

  HgoBackAppBar(
      {this.actions, Widget leading, Widget title, BuildContext context})
      : super(
          leading: BackButton(onPressed: () {
            Navigator.pop(context);
          }),
          title: Image.asset(
            'assets/logo2xxxhdpi.png',
            width: 50,
          ),
        );
}

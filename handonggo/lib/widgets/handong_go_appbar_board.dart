import 'package:flutter/material.dart';
import 'package:handong_go/blocs/board_bloc/board_state.dart';
import 'package:handong_go/pages/board/board_food_add.dart';
import 'package:handong_go/pages/board/board_free_add.dart';
import 'package:handong_go/pages/board/board_group_add.dart';
import 'package:handong_go/pages/board/board_market_add.dart';
import 'package:handong_go/utils/colors.dart';

class HgoBoardAppBar extends AppBar {
  final Color backgroundColor = mainColor;
  final double elevation = 0.0;
  final bool centerTitle = true;

  HgoBoardAppBar(
      {Widget title, Widget action, BuildContext context, BoardState state})
      : super(
            title: Image.asset(
              'assets/logo2xxxhdpi.png',
              width: 50,
            ),
            actions: <Widget>[
              IconButton(
                icon: Image.asset(
                  'assets/writexxxhdpi.png',
                  width: 20,
                  color: Colors.white,
                ),
                onPressed: () {
                  if (state.isFreeCategorySelected)
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => FreeBoardAdd(state)),
                    );
                  else if (state.isFoodCategorySelected)
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => FoodBoardAdd(state)),
                    );
                  else if (state.isGroupCategorySelected) {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => GroupBoardAdd(state)),
                    );
                  } else
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => MarketBoardAdd(state)),
                    );
                },
              ),
            ]);
}

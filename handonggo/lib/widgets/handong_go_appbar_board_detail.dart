import 'package:flutter/material.dart';
import 'package:handong_go/models/board_model.dart';
import 'package:handong_go/pages/board/board_food_edit.dart';
import 'package:handong_go/pages/board/board_free_edit.dart';
import 'package:handong_go/pages/board/board_group_edit.dart';
import 'package:handong_go/pages/board/board_market_edit.dart';

import 'package:handong_go/utils/colors.dart';

class HgoBoardDetailAppBar extends AppBar {
  final Color backgroundColor = mainColor;
  final double elevation = 0.0;
  final bool centerTitle = true;

  HgoBoardDetailAppBar(
      {Widget leading, Widget title, Widget action, BuildContext context, int index,
        FreeBoard freeBoard, FoodBoard foodBoard, GroupBoard groupBoard, MarketBoard marketBoard})
      : super(
            leading: BackButton(onPressed: () {
              Navigator.pop(context);
            }),
            title: Image.asset(
              'assets/logo2xxxhdpi.png',
              width: 50,
            ),
            actions: <Widget>[
              IconButton(
                icon: Image.asset(
                  'assets/writexxxhdpi.png',
                  width: 20,
                  color: Colors.white,
                ),
                onPressed: () {
                  if (index == 0){
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => FreeBoardEdit(board: freeBoard)),
                    );
                  }

                  if (index == 1){
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => FoodBoardEdit(board: foodBoard)),
                    );
                  }

                  if (index == 2) {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => GroupBoardEdit(board: groupBoard,)),
                    );
                  }

                  if (index == 3){
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => MarketBoardEdit(board: marketBoard)),
                    );
                  }
                },
              ),
            ]);
}

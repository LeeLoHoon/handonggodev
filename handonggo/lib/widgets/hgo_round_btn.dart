import 'package:flutter/material.dart';

class HgoRoundBtn extends StatelessWidget {
  final Color textColor;
  final Color backgroundColor;
  final String text;
  final width;

  const HgoRoundBtn(
      {Key key,
        @required this.textColor,
        @required this.backgroundColor,
        @required this.text,
        this.width})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        alignment: Alignment.center,
        padding: EdgeInsets.symmetric(vertical: 10),
        width: width ?? MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
            color: backgroundColor,
            borderRadius: BorderRadius.all(Radius.circular(10))),
        margin: EdgeInsets.symmetric(vertical: 15, horizontal: 18),
        child: Text(
          text,
          style: TextStyle(
              color: textColor, fontWeight: FontWeight.w800, fontSize: 17),
        ));
  }
}

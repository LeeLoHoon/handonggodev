import 'package:flutter/material.dart';

class HgoSearchBar extends StatelessWidget {
  final TextEditingController controller;
  final String hintText;

  const HgoSearchBar({Key key, this.controller, this.hintText}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 12, horizontal: 16),
      alignment: Alignment.center,
      child: Container(
        height: 50,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(30),
          color: Colors.white,
          border: Border.all(color: Colors.black)
        ),
        padding: EdgeInsets.symmetric(horizontal: 16, vertical: 10),
        child: Row(
          children: <Widget>[
            Container(
              width: MediaQuery.of(context).size.width * 0.7,
              child: TextField(
                  controller: controller,
                  decoration: InputDecoration(
                      contentPadding: new EdgeInsets.fromLTRB(0, 0, 0, 0),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10),
                          borderSide: BorderSide.none),
                      isDense: false,
                      hintText: hintText,
                      hintMaxLines: 1)),
              alignment: Alignment.center,
            ),
            Icon(Icons.search),

          ],
        ),
      ),
    );
  }
}

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:meta/meta.dart';

class User {
  final String uid;
  final String fcmToken;
  final String email;
  final String name;
  final String language;
  final String url;

  User({
    @required this.uid,
    @required this.fcmToken,
    @required this.email,
    @required this.name,
    @required this.language,
    @required this.url,
  });

  factory User.fromDs(DocumentSnapshot ds) {
    return User(
      uid: ds['uid'],
      fcmToken: ds['fcmToken'],
      email: ds['email'],
      name: ds['name'],
      language: ds['language'],
      url: ds['Url'],
    );
  }
}

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:meta/meta.dart';

class FreeBoard {
  final String bid;
  final int likeCnt;
  final List koComments;
  final List enComments;
  final List viComments;
  final List likers;
  final List title;
  final List content;
  final String image;
  final String owner;
  final String name;
  final String date;
  final String theme;

  FreeBoard({
    @required this.bid,
    @required this.likeCnt,
    @required this.koComments,
    @required this.enComments,
    @required this.viComments,
    @required this.likers,
    @required this.title,
    @required this.content,
    @required this.image,
    @required this.owner,
    @required this.name,
    @required this.date,
    @required this.theme,
  });

  factory FreeBoard.fromDs(DocumentSnapshot ds) {
    return FreeBoard(
        bid: ds['bid'],
        likeCnt: ds['likeCnt'],
        koComments: ds['koComments'],
        enComments: ds['enComments'],
        viComments: ds['viComments'],
        likers: ds['likers'],
        title: ds['title'],
        content: ds['content'],
        image: ds['image'],
        owner: ds['owner'],
        name: ds['name'],
        date: ds['date'],
        theme: ds['theme']);
  }
}

class FoodBoard {
  final String bid;
  final double value;
  final List koComments;
  final List enComments;
  final List viComments;
  final List judge;
  final List title;
  final List content;
  final String image;
  final String owner;
  final String name;
  final String date;
  final String theme;

  FoodBoard({
    @required this.bid,
    @required this.value,
    @required this.koComments,
    @required this.enComments,
    @required this.viComments,
    @required this.judge,
    @required this.title,
    @required this.content,
    @required this.image,
    @required this.owner,
    @required this.name,
    @required this.date,
    @required this.theme,
  });

  factory FoodBoard.fromDs(DocumentSnapshot ds) {
    return FoodBoard(
        bid: ds['bid'],
        value: ds['value'],
        koComments: ds['koComments'],
        enComments: ds['enComments'],
        viComments: ds['viComments'],
        judge: ds['judge'],
        title: ds['title'],
        content: ds['content'],
        image: ds['image'],
        owner: ds['owner'],
        name: ds['name'],
        date: ds['date'],
        theme: ds['theme']);
  }
}

class GroupBoard {
  final String bid;
  final int likeCnt;
  final List koComments;
  final List enComments;
  final List viComments;
  final List likers;
  final List title;
  final List content;
  final String image;
  final String owner;
  final String name;
  final String date;
  final String theme;

  GroupBoard({
    @required this.bid,
    @required this.likeCnt,
    @required this.koComments,
    @required this.enComments,
    @required this.viComments,
    @required this.likers,
    @required this.title,
    @required this.content,
    @required this.image,
    @required this.owner,
    @required this.name,
    @required this.date,
    @required this.theme,
  });

  factory GroupBoard.fromDs(DocumentSnapshot ds) {
    return GroupBoard(
        bid: ds['bid'],
        likeCnt: ds['likeCnt'],
        koComments: ds['koComments'],
        enComments: ds['enComments'],
        viComments: ds['viComments'],
        likers: ds['likers'],
        title: ds['title'],
        content: ds['content'],
        image: ds['image'],
        owner: ds['owner'],
        name: ds['name'],
        date: ds['date'],
        theme: ds['theme']);
  }
}

class MarketBoard {
  final String bid;
  final List koComments;
  final List enComments;
  final List viComments;
  final List likers;
  final List title;
  final List content;
  final String price;
  final String image;
  final String state;
  final String owner;
  final String name;
  final String date;
  final String theme;

  MarketBoard({
    @required this.bid,
    @required this.koComments,
    @required this.enComments,
    @required this.viComments,
    @required this.likers,
    @required this.title,
    @required this.price,
    @required this.content,
    @required this.image,
    @required this.state,
    @required this.owner,
    @required this.name,
    @required this.date,
    @required this.theme,
  });

  factory MarketBoard.fromDs(DocumentSnapshot ds) {
    return MarketBoard(
        bid: ds['bid'],
        koComments: ds['koComments'],
        enComments: ds['enComments'],
        viComments: ds['viComments'],
        likers: ds['likers'],
        title: ds['title'],
        price: ds['price'],
        content: ds['content'],
        image: ds['image'],
        state: ds['state'],
        owner: ds['owner'],
        name: ds['name'],
        date: ds['date'],
        theme: ds['theme']);
  }
}

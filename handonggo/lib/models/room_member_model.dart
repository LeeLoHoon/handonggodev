import 'package:meta/meta.dart';

class RoomMember {
  ///인력투입정보
  final String roomNo; //참여인원
  final String uid; //인건비(1인당)

  RoomMember({
    @required this.roomNo,
    @required this.uid,
  });

  factory RoomMember.fromDs(dynamic ds) {
    return RoomMember(
      roomNo: ds['title'],
      uid: ds['content'],
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'roomNo': this.roomNo,
      'uid': this.uid,
    };
  }
}

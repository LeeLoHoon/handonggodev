import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:handong_go/blocs/authentication_bloc/bloc.dart';
import 'package:handong_go/blocs/login_bloc/bloc.dart';
import 'package:handong_go/models/user_model.dart';
import 'package:handong_go/utils/colors.dart';
import 'package:handong_go/utils/user_repository.dart';
import 'package:handong_go/utils/user_util.dart';

class LoginScreen extends StatefulWidget {
  final UserRepository _userRepository;

  LoginScreen({Key key, @required UserRepository userRepository})
      : assert(userRepository != null),
        _userRepository = userRepository,
        super(key: key);

  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  LoginBloc _loginBloc;
  UserRepository get _userRepository => widget._userRepository;
  AuthenticationBloc _authenticationBloc;

  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();

  bool get isPopulated =>
      _emailController.text.isNotEmpty && _passwordController.text.isNotEmpty;

  @override
  void initState() {
    super.initState();
    _loginBloc = LoginBloc(
      userRepository: _userRepository,
    );
    _emailController.addListener(_onEmailChanged);
    _passwordController.addListener(_onPasswordChanged);
    _authenticationBloc = BlocProvider.of<AuthenticationBloc>(context);
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<Bloc<dynamic, AuthenticationState>, AuthenticationState>(
        bloc: _authenticationBloc,
        builder: (BuildContext context, AuthenticationState authState) {
          return BlocListener(
            bloc: _loginBloc,
            listener: (BuildContext context, LoginState state) async {
//              if (state.isFailure) {
//                Scaffold.of(context)
//                  ..hideCurrentSnackBar()
//                  ..showSnackBar(
//                    SnackBar(
//                      content: Row(
//                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                        children: [Text('Login Failure'), Icon(Icons.error)],
//                      ),
//                      backgroundColor: Colors.red,
//                    ),
//                  );
//              }
//              if (state.isSubmitting) {
//                Scaffold.of(context)
//                  ..hideCurrentSnackBar()
//                  ..showSnackBar(
//                    SnackBar(
//                      content: Row(
//                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                        children: [
//                          Text('Logging In...'),
//                          CircularProgressIndicator(),
//                        ],
//                      ),
//                    ),
//                  );
//              }
              if (state.isSuccess) {
                bool registered = false;
                print("넘어감?!!!!!!!!!!!!!!!!!!!!123");
                try {
                  UserUtil.setUser(User.fromDs(await Firestore.instance
                      .collection('User')
                      .document((await UserRepository().getUser()).uid)
                      .get()));

//                  print(UserUtil.getUser().nickName);
                    registered = true;
//                  registered = UserUtil.getUser().nickName == null
//                      ? false
//                      : UserUtil.getUser().nickName.length > 0;
                } catch (e) {}
                if (registered)
                  BlocProvider.of<AuthenticationBloc>(context).add(
                    LoggedIn(),
                  );
             //   else

//                  Navigator.push(
//                      context,
//                      MaterialPageRoute(
//                          builder: (BuildContext context) => ETStepper()));
              }
            },
            child: BlocBuilder(
              bloc: _loginBloc,
              builder: (BuildContext context, LoginState state) {
                return Scaffold(
                  body: Center(
                    child: Stack(
                      children: <Widget>[
                        Image.asset(
                          'assets/background.png',
                          width: MediaQuery.of(context).size.width ,
                          height: MediaQuery.of(context).size.height,
                        ),
                        Center(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              SizedBox(
                                  height: MediaQuery.of(context).size.height * 0.1),
                              Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Image.asset(
                                    'assets/logo.png',
                                    width: MediaQuery.of(context).size.width * 0.5,
                                  ),
                                  SizedBox(height: 20),

                                ],
                              ),
                              SizedBox(
                                  height: MediaQuery.of(context).size.height * 0.3),
                              Column(
                                children: <Widget>[
                                  loginBtn(
                                      text: "로그인",
                                      color: googleBntColor,
                                      action: () => _loginBloc.add(
                                            LoginWithGooglePressed(),
                                          )),
                                  SizedBox(height: 20),
                                  introduceAppBtn(
                                      text: "한동고를 소개합니다.",
                                      action: () => _loginBloc.add(
                                        LoginWithGooglePressed(),
                                      )),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                );
              },
            ),
          );
        });
  }

  Widget loginBtn({String text, Function action, Color color}) {
    return GestureDetector(
      onTap: () => action(),
      child: Container(
          height: 55,
          alignment: Alignment.center,
          width: MediaQuery.of(context).size.width * 0.6,
          decoration: new BoxDecoration(
              color: Colors.white,
              borderRadius: new BorderRadius.all(Radius.circular(30))),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Image.asset(
                'assets/googlexxxhdpi.png',
                width: MediaQuery.of(context).size.width * 0.09,
                height: MediaQuery.of(context).size.height * 0.09,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 50),
                child: Text(
                    text,
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 22,),
                ),
              ),
            ],
          )),
    );
  }

  Widget introduceAppBtn({String text, Function action,}) {
    return GestureDetector(
      onTap: () => action(),
      child: Container(
          height: 55,
          alignment: Alignment.center,
          width: MediaQuery.of(context).size.width * 0.6,
          decoration: new BoxDecoration(
              color: Colors.transparent,
              borderRadius: new BorderRadius.all(Radius.circular(30)),
              border: Border.all(color: Colors.white)),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                text,
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: 18, color: Colors.white),
              ),
              Icon(Icons.arrow_forward_ios,color: Colors.white,)
            ],
          )),
    );
  }

//  @override
//  void dispose() {
//    _loginBloc.dispose();
//    _emailController.dispose();
//    _passwordController.dispose();
//    super.dispose();
//  }

  void _onEmailChanged() {
    _loginBloc.add(
      EmailChanged(email: _emailController.text),
    );
  }

  void _onPasswordChanged() {
    _loginBloc.add(
      PasswordChanged(password: _passwordController.text),
    );
  }
}

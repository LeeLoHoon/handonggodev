import 'package:flutter/material.dart';

class SplashScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Stack(
          children: <Widget>[
            Image.asset(
              'assets/background.png',
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
            ),

            Center(
              child: Image.asset(
                'assets/logo.png',
                width: MediaQuery.of(context).size.width * 0.5,
              ),
            ),

          ],
        ),
      ),
    );
  }
}

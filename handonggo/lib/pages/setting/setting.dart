import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:handong_go/blocs/authentication_bloc/authentication_bloc.dart';
import 'package:handong_go/blocs/authentication_bloc/authentication_state.dart';
import 'package:handong_go/blocs/authentication_bloc/bloc.dart';
import 'package:handong_go/blocs/setting_bloc/bloc.dart';
import 'package:handong_go/pages/setting/select_language.dart';
import 'package:handong_go/utils/colors.dart';
import 'package:handong_go/widgets/handong_go_appbar.dart';

class Setting extends StatefulWidget {
  @override
  _SettingState createState() => _SettingState();
}

class _SettingState extends State<Setting> {
  AuthenticationBloc _authenticationBloc;
  SettingBloc _settingBloc;

  @override
  void initState() {
    // TODO: implement initState
    _authenticationBloc = BlocProvider.of<AuthenticationBloc>(context);
    _settingBloc = BlocProvider.of<SettingBloc>(context);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return  BlocListener(
      bloc: _settingBloc,
      listener: (BuildContext context, SettingState state) {
        if (state.logOutBtnPressed) {
          showDialog(
              context: context,
              builder: (BuildContext context) {
                return AlertDialog(
                  title: Text('로그아웃'),
                  content: Text('로그아웃 하시겠습니까?'),
                  actions: <Widget>[
                    OutlineButton(
                      child: Text(
                        'No',
                        style: TextStyle(color: mainColor),
                      ),
                      borderSide: BorderSide(style: BorderStyle.none),
                      onPressed: () {
                        _settingBloc.add(CancelLogout());
                        Navigator.pop(context);
                      },
                    ),
                    RaisedButton(
                      child: Text(
                        'Yes',
                        style: TextStyle(color: Colors.white),
                      ),
                      color: mainColor,
                      onPressed: () {
                        _settingBloc.add(DoLogout());
                      },
                    )
                  ],
                );
              });
        }
        if (state.doLogoutPressed) {
          _authenticationBloc.add(LoggedOut());
          Navigator.of(context).popUntil((route) => route.isFirst);
        }

        if (state.languageBtnPressed) {
          Navigator.of(context).push(MaterialPageRoute(
              builder: (BuildContext context) => BlocProvider<SettingBloc>.value(
                value: _settingBloc,
                child: SelectLanguagePage(),
              )));
        }
      },
        child: Scaffold(
            appBar: HgoAppBar(

            ),
            backgroundColor: Colors.white,
            body: BlocBuilder(
              bloc: _settingBloc,
              builder: (BuildContext context, SettingState state) {
                return Column(
                  children: <Widget>[
                    logoutButton(),
                    languageButton(),
                  ],
                );
              }
            )));
      }

  Widget logoutButton() {
    return InkWell(
      onTap: () {
        _settingBloc.add(LogOutBtnPressed());
      },
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 26, vertical: 20),
            child: Icon(
              Icons.input,

            ),
          ),
          Expanded(
            child: Text(
              '로그아웃',

            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 26),
            child: Icon(
              Icons.keyboard_arrow_right,
              size: 30,

            ),
          )
        ],
      ),
    );
  }

  Widget languageButton() {
    return InkWell(
      onTap: () {
        _settingBloc.add(LanguageBtnPressed());
      },
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 26, vertical: 20),
            child: Icon(
              Icons.language,

            ),
          ),
          Expanded(
            child: Text(
              '언어설정',

            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 26),
            child: Icon(
              Icons.keyboard_arrow_right,
              size: 30,

            ),
          )
        ],
      ),
    );
  }



}

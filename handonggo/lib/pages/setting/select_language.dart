import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:handong_go/blocs/authentication_bloc/authentication_bloc.dart';
import 'package:handong_go/blocs/authentication_bloc/authentication_state.dart';
import 'package:handong_go/blocs/authentication_bloc/bloc.dart';
import 'package:handong_go/blocs/setting_bloc/bloc.dart';
import 'package:handong_go/utils/LocaleUtil.dart';
import 'package:handong_go/utils/colors.dart';
import 'package:handong_go/widgets/handong_go_appbar.dart';

class SelectLanguagePage extends StatefulWidget {
  @override
  _SelectLanguagePageState createState() => _SelectLanguagePageState();
}

class _SelectLanguagePageState extends State<SelectLanguagePage> {
  AuthenticationBloc _authenticationBloc;
  SettingBloc _settingBloc;

  @override
  void initState() {
    // TODO: implement initState
    _authenticationBloc = BlocProvider.of<AuthenticationBloc>(context);
    _settingBloc = BlocProvider.of<SettingBloc>(context);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return  BlocListener(
        bloc: _settingBloc,
        listener: (BuildContext context, SettingState state) {
        },
        child: Scaffold(
            appBar: HgoAppBar(

            ),
            backgroundColor: Colors.white,
            body: BlocBuilder(
                bloc: _settingBloc,
                builder: (BuildContext context, SettingState state) {
                  return Column(
                    children: <Widget>[
                      language(),
                      saveBtn()
                    ],
                  );
                }
            )));
  }

  Widget language(){
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.all(20.0),
          child: InkWell(
            onTap: (){

            },
            child: Container(
              height: 80,
              width: 100,
              decoration: BoxDecoration(
                  color: mainColor,
                  borderRadius: BorderRadius.all(Radius.circular(15))),
              child: Center(child: Text("한국어",style: TextStyle(color: Colors.white),)),
            ),
          ),
        ),
        InkWell(
          onTap: (){
            print("이건 언어 ${LocaleUtil.getLocale()}");
          },
          child: Container(
            height: 80,
            width: 100,
            decoration: BoxDecoration(
                color: mainColor,
                borderRadius: BorderRadius.all(Radius.circular(15))),
            child: Center(child: Text("english",style: TextStyle(color: Colors.white),)),
          ),
        ),

      ],
    );
  }

  Widget saveBtn(){
    return  InkWell(
      onTap: (){
      },
      child: Container(
        height: 50,
        width: 100,
        decoration: BoxDecoration(
            color: mainColor,
            borderRadius: BorderRadius.all(Radius.circular(15))),
        child: Center(child: Text("저장",style: TextStyle(color: Colors.white),)),
      ),
    );
  }

}

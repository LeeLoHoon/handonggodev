import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:handong_go/models/board_model.dart';
import 'package:handong_go/models/user_model.dart';
import 'package:handong_go/pages/board/board_free_detail.dart';
import 'package:handong_go/pages/home/user_profile.dart';
import 'package:handong_go/pages/i18n/messages.dart';
import 'package:handong_go/utils/user_util.dart';
import 'package:handong_go/widgets/chat_room_item.dart';
import 'package:handong_go/widgets/column.builder.dart';
import 'package:handong_go/widgets/handong_go_appbar.dart';
import 'package:rxdart/rxdart.dart';

class Home extends StatefulWidget {
  final User user = UserUtil.getUser();

  @override
  _Home createState() => _Home();
}

class _Home extends State<Home> {
  final msg = Messages();
  BehaviorSubject<List<DocumentSnapshot>> _roomsController =
      BehaviorSubject<List<DocumentSnapshot>>();
  Stream<List<DocumentSnapshot>> get rooms => _roomsController;
  TextEditingController _searchQuery = new TextEditingController();

  var phoneSize;

  @override
  void initState() {
    super.initState();
    getChatList();
  }

  @override
  void dispose() {
    _roomsController.close();
    super.dispose();
  }

  void getChatList() {
    List<DocumentSnapshot> _docs = [];
    print(widget.user.uid);
    Firestore.instance
        .collection('ChatRooms')
        .orderBy("lastAdded", descending: true)
        .where("keywords", arrayContains: _searchQuery.text)
        .getDocuments()
        .then((QuerySnapshot qs) {
      qs.documents.forEach((DocumentSnapshot ds) {
        _docs.add(ds);
      });
      _roomsController.sink.add(_docs);
    });
  }

  @override
  Widget build(BuildContext context) {
    phoneSize = MediaQuery.of(context).size;
    return Scaffold(
      appBar: HgoAppBar(),
      backgroundColor: Colors.white,
      body: Padding(
        padding: const EdgeInsets.only(top: 4.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            UserProfile(
              name: "${widget.user.name}",
              email: "${widget.user.email}",
            ),
            Expanded(
              child: ListView(
                children: <Widget>[

                  _header(title: msg.appName, context: context),
//                  widget.user.language == "ko" ?
//                  _header(title: "참여중인 채팅방", context: context) :
//                      widget.user.language == "en" ?
//                          _header(title: "Participating Chat Room", context: context) :
//                          _header(title: "Phòng chat có mặt", context: context),
                  chatRoomList(UserUtil.getUser().uid),
                  SizedBox(
                    height: 16,
                  ),

//                  widget.user.language == "ko" ?
//                  _header(title: "인기 게시물 TOP 5", context: context) :
//                  widget.user.language == "en" ?
//                  _header(title: "Best Content Top 5", context: context) :
//                  _header(title: "Bài dăng được yêu thích", context: context),
                  _buildBoardBody(context, 0),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }


  Widget chatRoomList(String uid) {
    return StreamBuilder(
        stream: Firestore.instance
            .collection('ChatRooms')
            .where(uid, isEqualTo: true)
            .snapshots(),
        builder: (context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (!snapshot.hasData) {
            return Text('Loading...');
          } else {
            return Padding(
              padding: EdgeInsets.fromLTRB(5, 0, 5, 0),
              child: ColumnBuilder(
                  itemCount: snapshot.data.documents.length,
                  itemBuilder: (context, index) {
                    DocumentSnapshot ds = snapshot.data.documents[index];
                    return StreamBuilder<QuerySnapshot>(
                        stream: Firestore.instance
                            .collection('RoomMember')
                            .where("roomNo", isEqualTo: ds['roomNo'])
                            .snapshots(),
                        builder: (context, memberSnapshot) {
                          if (!memberSnapshot.hasData) return Container();
                          return ChatRoomItem(
                              ds: ds,
                              memberSnapshot: memberSnapshot,
                              context: context);
                        });
                  }),
            );
          }
        });
  }


  _header({String title, BuildContext context}) {
    return Container(
      height: phoneSize.height * .04,
      padding: EdgeInsets.only(
        left: 24.0,
        top: 4.0,
      ),
      child: Text(
        title,
        style: TextStyle(
            fontWeight: FontWeight.w600,
            fontSize: 18.0,
            letterSpacing: 0.8,
            fontFamily: 'hgoFont'),
      ),
    );
  }

  Widget _buildBoardBody(BuildContext context, int idx) {
    return StreamBuilder<QuerySnapshot>(
      stream: Firestore.instance
          .collection('FreeBoard')
          .orderBy("likeCnt", descending: true)
          .snapshots(),
      builder: (context, snapshot) {
        if (snapshot.hasError) return LinearProgressIndicator();
        if (!snapshot.hasData) return LinearProgressIndicator();

        return _buildList(context, snapshot.data.documents);
      },
    );
  }

  Widget _buildList(BuildContext context, List<DocumentSnapshot> snapshot) {
    return ColumnBuilder(
        itemCount: snapshot == null ? 0 : snapshot.length < 5 ? snapshot.length : 5,
        itemBuilder: (context, i) {
          return _buildListItem(context, snapshot[i]);
        });
  }

  Card _buildListItem(BuildContext context, DocumentSnapshot data) {
    final board = FreeBoard.fromDs(data);
    return Card(
        margin: EdgeInsets.fromLTRB(16.0, 4.0, 16.0, 4.0),
        child: ListTile(
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => FreeBoardDetail(bid: board.bid)));
            },
            title: Text(
              board.title[0],
              style: TextStyle(fontSize: 20, color: Colors.black),
            ),
            subtitle: Text(board.content[0]),
            trailing: SizedBox(
              width: 50,
              child: Column(
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Icon(
                        Icons.favorite,
                        color: Colors.red,
                        size: 20,
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      Text(board.likeCnt.toString()),
                    ],
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  Row(
                    children: <Widget>[
                      Icon(
                        Icons.message,
                        color: Colors.blue,
                        size: 20,
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      Text(board.koComments.length.toString()),
                    ],
                  )
                ],
              ),
            )));
  }
}

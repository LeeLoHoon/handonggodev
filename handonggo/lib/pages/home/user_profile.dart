import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';

class UserProfile extends StatelessWidget {
  var phoneSize;
  var color;
  final String name;
  final String email;

  UserProfile({this.name, this.email});

  @override
  Widget build(BuildContext context) {
    phoneSize = MediaQuery.of(context).size;
    color = "0x" + email.substring(0, 8);
    return Container(
      margin: EdgeInsets.fromLTRB(12.0, 12.0, 12.0, 12.0),
      padding: EdgeInsets.symmetric(
          vertical: phoneSize.width * .04, horizontal: phoneSize.width * .02),
      decoration: BoxDecoration(
        border: Border.all(
          width: 1,
          color: Colors.grey,
        ),
        borderRadius: BorderRadius.all(
          Radius.circular(15.0),
        ),
      ),
      child: Row(
        children: <Widget>[
          Column(
            children: <Widget>[
              Container(
                  margin: EdgeInsets.symmetric(horizontal: 12.0, vertical: 4.0),
                  width: 56.0,
                  height: 56.0,
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                  ),
                  child: Container(
                    decoration: BoxDecoration(
                        color: Color(int.parse(color)),
                        shape: BoxShape.circle
                    ),
                  ),
              ),
            ],
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.end,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Row(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: <Widget>[
                  Text(
                    name,
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.w700,
                      fontFamily: 'hgoFont'
                    ),
                  ),
                ],
              ),
              SizedBox(height: 6.0),
              AutoSizeText(
                email,
                maxLines: 1,
                minFontSize: 1,
              ),
            ],
          ),
        ],
      ),
    );
  }
}

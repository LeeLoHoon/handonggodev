import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:handong_go/models/board_model.dart';
import 'package:handong_go/models/user_model.dart';
import 'package:handong_go/utils/translate.dart';
import 'package:handong_go/utils/user_util.dart';
import 'package:handong_go/widgets/handong_go_appbar_back.dart';
import 'package:handong_go/widgets/handong_go_appbar_board_detail.dart';
import 'package:intl/intl.dart';

enum SingingCharacter { veryGood, good, nomal, bad, veryBad }

final FirebaseAuth _auth = FirebaseAuth.instance;
String _userId;

class FoodBoardDetail extends StatefulWidget {
  final String bid;
  FoodBoardDetail({this.bid});

  @override
  FoodBoardDetailState createState() => FoodBoardDetailState(bid: bid);
}

class FoodBoardDetailState extends State<FoodBoardDetail> {
  final String bid;
  FoodBoardDetailState({this.bid});

  User user = UserUtil.getUser();
  final _formKey = GlobalKey<FormState>();
  String _comment;
  double _value = 1.0;
  SingingCharacter _character = SingingCharacter.veryBad;

  String _koComment;
  String _enComment;
  String _viComment;

  @override
  Widget build(BuildContext context) {
    FirebaseAuth.instance.currentUser().then((user) {
      _userId = user.uid;
    });

    return StreamBuilder(
        stream: Firestore.instance
            .collection('FoodBoard')
            .document(bid)
            .snapshots(),
        builder: (context, snapshot) {
          if (snapshot.hasError) return LinearProgressIndicator();
          if (!snapshot.hasData)
            return LinearProgressIndicator();
          else {
            FoodBoard board = FoodBoard.fromDs(snapshot.data);
            return WillPopScope(
                onWillPop: () async => false,
                child: MaterialApp(
                  home: Scaffold(
                    appBar: user.uid == board.owner
                        ? HgoBoardDetailAppBar(
                            context: this.context,
                            foodBoard: board,
                            index: 1,
                          )
                        : HgoBackAppBar(context: this.context),
                    body: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        SizedBox(
                          height: 20,
                        ),
                        Expanded(
                            child: ListView.builder(
                                itemCount: board.viComments.length + 1,
                                itemBuilder: (context, i) {
                                  if (i == 0) {
                                    return Column(
                                      children: <Widget>[
                                        Row(
                                          children: <Widget>[
                                            SizedBox(
                                              width: 20,
                                            ),
                                            Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: <Widget>[
                                                Text(board.name),
                                                Text(board.date
                                                    .substring(0, 13)),
                                              ],
                                            ),
                                            SizedBox(
                                              width: 150,
                                            ),
                                            RaisedButton(
                                              child: Text("평가하기"),
                                              onPressed: () {
                                                if (board.judge
                                                    .contains(user.uid)) {
                                                  showDialog(
                                                      context: context,
                                                      builder: (BuildContext
                                                          context) {
                                                        return AlertDialog(
                                                          content: Text(
                                                              "이미 평가하셨습니다."),
                                                          contentPadding:
                                                              EdgeInsets
                                                                  .fromLTRB(
                                                                      28.0,
                                                                      32.0,
                                                                      24.0,
                                                                      8.0),
                                                          actions: <Widget>[
                                                            FlatButton(
                                                              child: new Text(
                                                                "확인",
                                                                style: TextStyle(
                                                                    color: Colors
                                                                        .black,
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .w300,
                                                                    fontSize:
                                                                        15.0),
                                                              ),
                                                              onPressed: () =>
                                                                  Navigator.of(
                                                                          context)
                                                                      .pop(),
                                                            )
                                                          ],
                                                          shape: RoundedRectangleBorder(
                                                              borderRadius: BorderRadius
                                                                  .all(Radius
                                                                      .circular(
                                                                          24.0))),
                                                        );
                                                      });
                                                } else {
                                                  showDialog(
                                                      context: context,
                                                      builder: (BuildContext
                                                          context) {
                                                        return AlertDialog(
                                                          content:
                                                              StatefulBuilder(
                                                            builder: (BuildContext
                                                                    context,
                                                                StateSetter
                                                                    setState) {
                                                              return SizedBox(
                                                                  height: 260,
                                                                  child: Column(
                                                                    crossAxisAlignment:
                                                                        CrossAxisAlignment
                                                                            .start,
                                                                    children: <
                                                                        Widget>[
                                                                      Text(
                                                                          "평가해 주세요."),
                                                                      Row(
                                                                        mainAxisAlignment:
                                                                            MainAxisAlignment.start,
                                                                        children: <
                                                                            Widget>[
                                                                          Radio(
                                                                              value: SingingCharacter.veryBad,
                                                                              groupValue: _character,
                                                                              onChanged: (SingingCharacter value) {
                                                                                setState(() {
                                                                                  _character = value;
                                                                                  _value = 1.0;
                                                                                });
                                                                              }),
                                                                          Icon(
                                                                            Icons.star,
                                                                            color:
                                                                                Colors.yellow,
                                                                          ),
                                                                        ],
                                                                      ),
                                                                      Row(
                                                                        children: <
                                                                            Widget>[
                                                                          Radio(
                                                                              value: SingingCharacter.bad,
                                                                              groupValue: _character,
                                                                              onChanged: (SingingCharacter value) {
                                                                                setState(() {
                                                                                  _character = value;
                                                                                  _value = 2.0;
                                                                                });
                                                                              }),
                                                                          Icon(
                                                                            Icons.star,
                                                                            color:
                                                                                Colors.yellow,
                                                                          ),
                                                                          Icon(
                                                                            Icons.star,
                                                                            color:
                                                                                Colors.yellow,
                                                                          ),
                                                                        ],
                                                                      ),
                                                                      Row(
                                                                        children: <
                                                                            Widget>[
                                                                          Radio(
                                                                              value: SingingCharacter.nomal,
                                                                              groupValue: _character,
                                                                              onChanged: (SingingCharacter value) {
                                                                                setState(() {
                                                                                  _character = value;
                                                                                  _value = 3.0;
                                                                                });
                                                                              }),
                                                                          Icon(
                                                                            Icons.star,
                                                                            color:
                                                                                Colors.yellow,
                                                                          ),
                                                                          Icon(
                                                                            Icons.star,
                                                                            color:
                                                                                Colors.yellow,
                                                                          ),
                                                                          Icon(
                                                                            Icons.star,
                                                                            color:
                                                                                Colors.yellow,
                                                                          ),
                                                                        ],
                                                                      ),
                                                                      Row(
                                                                        children: <
                                                                            Widget>[
                                                                          Radio(
                                                                              value: SingingCharacter.good,
                                                                              groupValue: _character,
                                                                              onChanged: (SingingCharacter value) {
                                                                                setState(() {
                                                                                  _character = value;
                                                                                  _value = 4.0;
                                                                                });
                                                                              }),
                                                                          Icon(
                                                                            Icons.star,
                                                                            color:
                                                                                Colors.yellow,
                                                                          ),
                                                                          Icon(
                                                                            Icons.star,
                                                                            color:
                                                                                Colors.yellow,
                                                                          ),
                                                                          Icon(
                                                                            Icons.star,
                                                                            color:
                                                                                Colors.yellow,
                                                                          ),
                                                                          Icon(
                                                                            Icons.star,
                                                                            color:
                                                                                Colors.yellow,
                                                                          ),
                                                                        ],
                                                                      ),
                                                                      Row(
                                                                        children: <
                                                                            Widget>[
                                                                          Radio(
                                                                              value: SingingCharacter.veryGood,
                                                                              groupValue: _character,
                                                                              onChanged: (SingingCharacter value) {
                                                                                setState(() {
                                                                                  _character = value;
                                                                                  _value = 5.0;
                                                                                });
                                                                              }),
                                                                          Icon(
                                                                            Icons.star,
                                                                            color:
                                                                                Colors.yellow,
                                                                          ),
                                                                          Icon(
                                                                            Icons.star,
                                                                            color:
                                                                                Colors.yellow,
                                                                          ),
                                                                          Icon(
                                                                            Icons.star,
                                                                            color:
                                                                                Colors.yellow,
                                                                          ),
                                                                          Icon(
                                                                            Icons.star,
                                                                            color:
                                                                                Colors.yellow,
                                                                          ),
                                                                          Icon(
                                                                            Icons.star,
                                                                            color:
                                                                                Colors.yellow,
                                                                          ),
                                                                        ],
                                                                      ),
                                                                    ],
                                                                  ));
                                                            },
                                                          ),
                                                          contentPadding:
                                                              EdgeInsets
                                                                  .fromLTRB(
                                                                      28.0,
                                                                      32.0,
                                                                      24.0,
                                                                      8.0),
                                                          actions: <Widget>[
                                                            Container(
                                                              child: FlatButton(
                                                                  child:
                                                                      new Text(
                                                                    "평가하기",
                                                                    style: TextStyle(
                                                                        color: Colors
                                                                            .black,
                                                                        fontWeight:
                                                                            FontWeight
                                                                                .w300,
                                                                        fontSize:
                                                                            15.0),
                                                                  ),
                                                                  onPressed:
                                                                      () {
                                                                    print(
                                                                        _value);
                                                                    _evaluate(
                                                                        _value);
                                                                    Navigator.pop(
                                                                        context);
                                                                  }),
                                                            )
                                                          ],
                                                          shape: RoundedRectangleBorder(
                                                              borderRadius: BorderRadius
                                                                  .all(Radius
                                                                      .circular(
                                                                          24.0))),
                                                        );
                                                      });
                                                }
                                              },
                                            ),
                                          ],
                                        ),
                                        SizedBox(
                                          height: 30,
                                        ),
                                        Container(
                                            padding: const EdgeInsets.fromLTRB(
                                                20, 0, 0, 0),
                                            alignment: Alignment.topLeft,
                                            child: Text(
                                                user.language == 'ko'
                                                    ? board.title[0]
                                                    : user.language == 'en'
                                                        ? board.title[1]
                                                        : board.title[2],
                                                style: TextStyle(
                                                    fontWeight: FontWeight.bold,
                                                    fontSize: 20,
                                                    color: Colors.black))),
                                        SizedBox(height: 10),
                                        Container(
                                            padding: const EdgeInsets.fromLTRB(
                                                20, 0, 0, 0),
                                            alignment: Alignment.topLeft,
                                            child: Text(
                                                user.language == 'ko'
                                                    ? board.content[0]
                                                    : user.language == 'en'
                                                        ? board.content[1]
                                                        : board.content[2],
                                                style: TextStyle(
                                                    fontSize: 18,
                                                    color: Colors.black))),
                                        SizedBox(height: 10),
                                        board.image == null
                                            ? Text("")
                                            : Container(
                                                alignment: Alignment.topLeft,
                                                padding:
                                                    const EdgeInsets.fromLTRB(
                                                        20, 0, 0, 0),
                                                child: Image.network(
                                                  board.image,
                                                  width: 100,
                                                  height: 100,
                                                )),
                                        Row(
                                          children: <Widget>[
                                            SizedBox(
                                              width: 20,
                                            ),
                                            Icon(
                                              Icons.star,
                                              color: Colors.yellow,
                                              size: 20,
                                            ),
                                            SizedBox(
                                              width: 10,
                                            ),
                                            board.judge.length == null
                                                ? Text("")
                                                : Text(board.judge.length == 0
                                                    ? "0.0"
                                                    : "${(board.value / board.judge.length).toStringAsFixed(1)}"),
                                            SizedBox(
                                              width: 20,
                                            ),
                                            Icon(
                                              Icons.message,
                                              color: Colors.blue,
                                              size: 20,
                                            ),
                                            SizedBox(
                                              width: 10,
                                            ),
                                            Text(board.koComments.length
                                                .toString()),
                                          ],
                                        ),
                                        SizedBox(
                                          height: 30,
                                        )
                                      ],
                                    );
                                  } else
                                    return Row(
                                      children: <Widget>[
                                        SizedBox(
                                          width: 10,
                                        ),
                                        Text(user.language == 'ko'
                                            ? board.koComments[i - 1].toString()
                                            : user.language == 'en'
                                                ? board.enComments[i - 1]
                                                    .toString()
                                                : board.viComments[i - 1]
                                                    .toString()),
                                        SizedBox(
                                          height: 20,
                                        )
                                      ],
                                    );
                                })),
                        Form(
                          key: _formKey,
                          child: Row(
                            children: <Widget>[
                              SizedBox(
                                width: 10,
                              ),
                              Container(
                                child: Expanded(
                                    child: TextFormField(
                                  validator: (value) {
                                    return value.isEmpty
                                        ? 'Enter Contents'
                                        : null;
                                  },
                                  onChanged: (text) {
                                    _comment = text;
                                  },
                                  decoration: InputDecoration(
                                    border: OutlineInputBorder(),
                                    alignLabelWithHint: true,
                                    hintText: '댓글을 입력하세요.',
                                    hintStyle: TextStyle(
                                        fontSize: 15, color: Colors.grey),
                                  ),
                                )),
                              ),
                              IconButton(
                                icon: Icon(Icons.send),
                                onPressed: () async {
                                  if (_formKey.currentState.validate()) {
                                    _formKey.currentState.reset();
                                    DateTime now = DateTime.now();
                                    String formattedDate =
                                        DateFormat("yyyy.MM.dd.HH.mm.ss")
                                            .format(now);
                                    if (user.language == "ko") {
                                      _koComment = _comment;

                                      _enComment =
                                          await Translate('ko', _koComment)
                                              .translateKoToEn();

                                      _viComment =
                                          await Translate('ko', _koComment)
                                              .translateKoToVi();
                                    } else if (user.language == 'en') {
                                      _enComment = _comment;

                                      _koComment =
                                          await Translate('en', _enComment)
                                              .translateEnToKo();
                                      _viComment =
                                          await Translate('ko', _koComment)
                                              .translateKoToVi();
                                    } else {
                                      _viComment = _comment;

                                      _koComment =
                                          await Translate('vi', _viComment)
                                              .translateViToKo();

                                      _enComment =
                                          await Translate('ko', _koComment)
                                              .translateKoToEn();
                                    }
                                    Firestore.instance
                                        .collection('FoodBoard')
                                        .document(board.bid)
                                        .updateData({
                                      'koComments': FieldValue.arrayUnion([
                                        formattedDate.substring(5, 7) +
                                            '/' +
                                            formattedDate.substring(8, 10) +
                                            " " +
                                            formattedDate.substring(11, 13) +
                                            ":" +
                                            formattedDate.substring(14, 16) +
                                            "   " +
                                            _koComment
                                      ])
                                      //create , modify 추가
                                    });
                                    Firestore.instance
                                        .collection('FoodBoard')
                                        .document(board.bid)
                                        .updateData({
                                      'enComments': FieldValue.arrayUnion([
                                        formattedDate.substring(5, 7) +
                                            '/' +
                                            formattedDate.substring(8, 10) +
                                            " " +
                                            formattedDate.substring(11, 13) +
                                            ":" +
                                            formattedDate.substring(14, 16) +
                                            "   " +
                                            _enComment
                                      ])
                                      //create , modify 추가
                                    });
                                    Firestore.instance
                                        .collection('FoodBoard')
                                        .document(board.bid)
                                        .updateData({
                                      'viComments': FieldValue.arrayUnion([
                                        formattedDate.substring(5, 7) +
                                            '/' +
                                            formattedDate.substring(8, 10) +
                                            " " +
                                            formattedDate.substring(11, 13) +
                                            ":" +
                                            formattedDate.substring(14, 16) +
                                            "   " +
                                            _viComment
                                      ])
                                      //create , modify 추가
                                    });
                                  }
                                },
                              )
                            ],
                          ),
                        ),
                        SizedBox(
                          height: 20,
                        )
                      ],
                    ),
                  ),
                ));
          }
        });
  }

  Future _evaluate(double value) async {
    Firestore.instance
        .collection('FoodBoard')
        .document(bid)
        .updateData({'value': FieldValue.increment(value)});

    Firestore.instance.collection('FoodBoard').document(bid).updateData({
      'judge': FieldValue.arrayUnion([user.uid])
    });
  }
}

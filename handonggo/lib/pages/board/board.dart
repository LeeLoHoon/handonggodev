import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:handong_go/blocs/board_bloc/bloc.dart';
import 'package:handong_go/models/board_model.dart';
import 'package:handong_go/models/user_model.dart';
import 'package:handong_go/pages/board/board_free_detail.dart';
import 'package:handong_go/pages/board/board_group_detail.dart';
import 'package:handong_go/utils/colors.dart';
import 'package:handong_go/utils/user_util.dart';
import 'package:handong_go/widgets/handong_go_appbar_board.dart';
import 'package:handong_go/widgets/search_bar.dart';

import 'board_food_detail.dart';
import 'board_market_detail.dart';

class Board extends StatefulWidget {
  @override
  _BoardState createState() => _BoardState();
}

class _BoardState extends State<Board> {
  TextEditingController _searchQuery = new TextEditingController();
  BoardBloc _boardBloc;

  User user = UserUtil.getUser();

  @override
  void initState() {
    // TODO: implement initState
    _boardBloc = BoardBloc();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener(
      bloc: _boardBloc,
      listener: (BuildContext context, BoardState state) {},
      child: BlocBuilder(
          bloc: _boardBloc,
          builder: (BuildContext context, BoardState state) {
            return Scaffold(
              appBar: HgoBoardAppBar(
                context: this.context,
                state: state,
              ),
              backgroundColor: Colors.white,
              body: Column(
                children: <Widget>[
                  HgoSearchBar(
                    controller: _searchQuery,
                    hintText: user.language == "ko" ? "게시물을 검색해보세요":
                    user.language == "en" ?
                    "Search" : "Tìm kiếm.",
                  ),
                  boardCategories(state),
                  SizedBox(
                    height: 20,
                  ),
                  element(context, state)
                ],
              ),
            );
          }),
    );
  }

  Widget element(BuildContext context, BoardState state) {
    if (state.isFreeCategorySelected)
      return _buildBody(context, 0);
    else if (state.isFoodCategorySelected)
      return _buildBody(context, 1);
    else if (state.isGroupCategorySelected)
      return _buildBody(context, 2);
    else
      return _buildBody(context, 3);
  }

  Widget _buildBody(BuildContext context, int idx) {
    if (idx == 0) {
      return StreamBuilder<QuerySnapshot>(
        stream: Firestore.instance.collection('FreeBoard').snapshots(),
        builder: (context, snapshot) {
          if (snapshot.hasError) return LinearProgressIndicator();
          if (!snapshot.hasData) return LinearProgressIndicator();

          return _buildList(context, snapshot.data.documents, idx);
        },
      );
    } else if (idx == 1) {
      return StreamBuilder<QuerySnapshot>(
        stream: Firestore.instance.collection('FoodBoard').snapshots(),
        builder: (context, snapshot) {
          if (snapshot.hasError) return LinearProgressIndicator();
          if (!snapshot.hasData) return LinearProgressIndicator();

          return _buildList(context, snapshot.data.documents, idx);
        },
      );
    } else if (idx == 2) {
      return StreamBuilder<QuerySnapshot>(
        stream: Firestore.instance.collection('GroupBoard').snapshots(),
        builder: (context, snapshot) {
          if (snapshot.hasError) return LinearProgressIndicator();
          if (!snapshot.hasData) return LinearProgressIndicator();

          return _buildList(context, snapshot.data.documents, idx);
        },
      );
    } else {
      return StreamBuilder<QuerySnapshot>(
        stream: Firestore.instance.collection('MarketBoard').snapshots(),
        builder: (context, snapshot) {
          if (snapshot.hasError) return LinearProgressIndicator();
          if (!snapshot.hasData) return LinearProgressIndicator();

          return _buildList(context, snapshot.data.documents, idx);
        },
      );
    }
  }

  Widget _buildList (
      BuildContext context, List<DocumentSnapshot> snapshot, int idx) {
    return Expanded(
        child: ListView.builder(
            itemCount: snapshot.length == null ? 0 : snapshot.length,
            itemBuilder: (context, i) {
              return _buildListItem(context, snapshot[i], idx);
            }
            )
    );
  }

  Card _buildListItem(BuildContext context, DocumentSnapshot data, int idx) {
    if (idx == 0) {
      final board = FreeBoard.fromDs(data);
      return Card(
          child: ListTile(
              //Todo: 오너는 수정가능하도록
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => FreeBoardDetail(bid: board.bid)));
              },
              title: user.language == 'ko'
                  ? Text(
                      board.title[0],
                      style: TextStyle(fontSize: 20, color: Colors.black),
                    )
                  : user.language == 'en'
                      ? Text(
                          board.title[1],
                          style: TextStyle(fontSize: 20, color: Colors.black),
                        )
                      : Text(
                          board.title[2],
                          style: TextStyle(fontSize: 20, color: Colors.black),
                        ),
              subtitle: user.language == 'ko'
                  ? Text(
                      board.content[0],
                    )
                  : user.language == 'en'
                      ? Text(
                          board.content[1],
                        )
                      : Text(
                          board.content[2],
                        ),
              trailing: SizedBox(
                width: 60,
                child: Column(
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Icon(
                          Icons.favorite,
                          color: Colors.red,
                          size: 20,
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        //Todo: 여기서 에러 잡아야함
                        Text(board.likeCnt.toString()),
                      ],
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    Row(
                      children: <Widget>[
                        Icon(
                          Icons.message,
                          color: Colors.blue,
                          size: 20,
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        Text(board.koComments.length.toString()),
                      ],
                    )
                  ],
                ),
              )

//      trailing: Image.network(board.image),
              ));
    } else if (idx == 1) {
      final board = FoodBoard.fromDs(data);
//      String average = (board.value / board.judge.length).toString();
      try {
        return Card(
            child: ListTile(
                //Todo: 오너는 수정가능하도록
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) =>
                              FoodBoardDetail(bid: board.bid)));
                },
                title: Text(
                  user.language == 'ko'
                      ? board.title[0]
                      : user.language == 'en' ? board.title[1] : board.title[2],
                  style: TextStyle(fontSize: 20, color: Colors.black),
                ),
                subtitle: Text(
                  user.language == 'ko'
                      ? board.content[0]
                      : user.language == 'en'
                          ? board.content[1]
                          : board.content[2],
                ),
                trailing: SizedBox(
                  width: 60,
                  child: Column(
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          Icon(
                            Icons.star,
                            color: Colors.yellow,
                            size: 20,
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          //Todo: 여기서 에러 잡아야함
//                        board.judge.length

                          board.judge.length == null
                              ? Text("")
                              : Text(board.judge.length == 0
                                  ? "0.0"
                                  : "${(board.value / board.judge.length).toStringAsFixed(1)}")
                        ],
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      Row(
                        children: <Widget>[
                          Icon(
                            Icons.message,
                            color: Colors.blue,
                            size: 20,
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          Text(board.koComments.length.toString()),
                        ],
                      )
                    ],
                  ),
                )

//      trailing: Image.network(board.image),
                ));
      } catch (e, s) {
//        print(s);
      }
    } else if (idx == 2) {
      final board = GroupBoard.fromDs(data);
      return Card(
          child: ListTile(
              //Todo: 오너는 수정가능하도록
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) =>
                            GroupBoardDetail(bid: board.bid)));
              },
              title: Text(
                user.language == 'ko'
                    ? board.title[0]
                    : user.language == 'en' ? board.title[1] : board.title[2],
                style: TextStyle(fontSize: 20, color: Colors.black),
              ),
              subtitle: Text(
                user.language == 'ko'
                    ? board.content[0]
                    : user.language == 'en'
                        ? board.content[1]
                        : board.content[2],
              ),
              trailing: SizedBox(
                width: 60,
                child: Column(
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Icon(
                          Icons.favorite,
                          color: Colors.red,
                          size: 20,
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        //Todo: 여기서 에러 잡아야함
                        Text(board.likeCnt.toString()),
                      ],
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    Row(
                      children: <Widget>[
                        Icon(
                          Icons.message,
                          color: Colors.blue,
                          size: 20,
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        Text(board.koComments.length.toString()),
                      ],
                    )
                  ],
                ),
              )

//      trailing: Image.network(board.image),
              ));
    } else {
      final board = MarketBoard.fromDs(data);
      return Card(
          child: ListTile(
              //Todo: 오너는 수정가능하도록
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) =>
                            MarketBoardDetail(bid: board.bid)));
              },
              leading: board.image == null
                  ? Image.asset(
                      "assets/logo.png",
                      scale: 50,
                      color: Colors.black,
                    )
                  : Image.network(
                      board.image,
                      scale: 50,
                    ),
              title: Text(
                user.language == 'ko'
                    ? board.title[0]
                    : user.language == 'en' ? board.title[1] : board.title[2],
                style: TextStyle(fontSize: 20, color: Colors.black),
              ),
              subtitle: Text(
                user.language == 'ko'
                    ? board.content[0]
                    : user.language == 'en'
                        ? board.content[1]
                        : board.content[2],
              ),
              trailing: SizedBox(
                width: 90,
                child: Column(
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Icon(
                          Icons.star,
                          color: Colors.yellow,
                          size: 20,
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        //Todo: 여기서 에러 잡아야함
                        Text("${board.state}"),
                      ],
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    Row(
                      children: <Widget>[
                        Icon(
                          Icons.message,
                          color: Colors.blue,
                          size: 20,
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        Text(board.koComments.length.toString()),
                      ],
                    )
                  ],
                ),
              )

//      trailing: Image.network(board.image),
              ));
    }
  }

  Widget boardCategories(BoardState state) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        SizedBox(
          width: 5,
        ),
        InkWell(
          onTap: () {
            _boardBloc.add(IsFreeCategorySelected());
          },
          child: buttonTheme(user.language == "ko" ? "자유":
          user.language == "en" ?
          "Free" : "Tự do", state.isFreeCategorySelected),
        ),
        InkWell(
          onTap: () {
            _boardBloc.add(IsFoodCategorySelected());
          },
          child: buttonTheme(user.language == "ko" ? "음식":
          user.language == "en" ?
          "Food" : "thức ăn", state.isFoodCategorySelected),
        ),

        InkWell(
          onTap: () {
            _boardBloc.add(IsGroupCategorySelected());
          },
          child: buttonTheme(user.language == "ko" ? "그룹":
          user.language == "en" ?
          "Group" : "nhóm", state.isGroupCategorySelected),
        ),
        InkWell(
            onTap: () {
              _boardBloc.add(IsMarketCategorySelected());
            },
            child: buttonTheme(user.language == "ko" ? "장터":
            user.language == "en" ?
            "Market" : "chợ", state.isMarketCategorySelected)),
        SizedBox(
          width: 5,
        ),
      ],
    );
  }

  Widget buttonTheme(String buttonTitle, bool isSelected) {
    return Container(
      height: 35,
      width: MediaQuery.of(context).size.width * 0.2,
      child: Center(
          child: Text(
        buttonTitle,
        style: TextStyle(
            fontSize: 20, color: isSelected ? Colors.white : mainColor),
      )),
      decoration: BoxDecoration(
          color: isSelected ? mainColor : Colors.white,
          border: Border.all(
            color: mainColor,
          ),
          borderRadius: BorderRadius.all(Radius.circular(20))),
    );
  }
}

import 'dart:io';
import 'dart:math';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:handong_go/models/board_model.dart';
import 'package:handong_go/models/user_model.dart';
import 'package:handong_go/utils/colors.dart';
import 'package:handong_go/utils/translate.dart';
import 'package:handong_go/utils/user_util.dart';
import 'package:image_picker/image_picker.dart';

class GroupBoardEdit extends StatefulWidget {
  final GroupBoard board;
  GroupBoardEdit({this.board});

  @override
  GroupBoardEditState createState() {
    return GroupBoardEditState(board: board);
  }
}

class GroupBoardEditState extends State<GroupBoardEdit> {
  GroupBoard board;
  final TextEditingController _titleController = TextEditingController();
  final TextEditingController _contentController = TextEditingController();
  GroupBoardEditState({this.board});

  User user = UserUtil.getUser();
  String pw;
  String time;
  bool check;
  final _formKey = GlobalKey<FormState>();
  File _image;
  Future<String> _imageURL;
  String _imageURLend;
  FirebaseApp app;
  FirebaseStorage storage =
      new FirebaseStorage(storageBucket: 'gs://handonggo-b3d60.appspot.com/');

  String _koTitle, _koContent;
  String _enTitle, _enContent;
  String _viTitle, _viContent;

  @override
  Widget build(BuildContext context) {
    _titleController.text = user.language == 'ko'
        ? board.title[0]
        : user.language == 'en' ? board.title[1] : board.title[2];
    _contentController.text = user.language == 'ko'
        ? board.content[0]
        : user.language == 'en' ? board.content[1] : board.content[2];
    return WillPopScope(
        onWillPop: () async => false,
        child: MaterialApp(
            home: Scaffold(
          appBar: AppBar(
            leading: new IconButton(
              icon: BackButton(
                color: Colors.white,
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
            ),
            backgroundColor: mainColor,
            automaticallyImplyLeading: false,
            elevation: 0,
            title: Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Container(
                    width: MediaQuery.of(context).size.width * 0.6,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        AutoSizeText(
                          user.language == "ko" ? "그룹 게시판 수정":
                          user.language == "en" ?
                          "Group Board Edit" : "Biên tập",
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 18,
                              fontWeight: FontWeight.w600),
                          textAlign: TextAlign.center,
                          maxLines: 1,
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            actions: <Widget>[
              Container(
                margin: const EdgeInsets.all(10.0),
                alignment: Alignment(-3, 0),
                child: GestureDetector(
                  onTap: () async {
                    if (_formKey.currentState.validate()) {
                      Navigator.pop(context);

                      if (user.language == "ko") {
                        _koTitle = _titleController.text;
                        _koContent = _contentController.text;
                        _enTitle =
                            await Translate('ko', _koTitle).translateKoToEn();
                        _enContent =
                            await Translate('ko', _koContent).translateKoToEn();
                        _viTitle =
                            await Translate('ko', _koTitle).translateKoToVi();
                        _viContent =
                            await Translate('ko', _koContent).translateKoToVi();
                      } else if (user.language == 'en') {
                        _enTitle = _titleController.text;
                        _enContent = _contentController.text;
                        _koTitle =
                            await Translate('en', _enTitle).translateEnToKo();
                        _koContent =
                            await Translate('en', _enContent).translateEnToKo();
                        _viTitle =
                            await Translate('ko', _koTitle).translateKoToVi();
                        _viContent =
                            await Translate('ko', _koContent).translateKoToVi();
                      } else {
                        _viTitle = _titleController.text;
                        _viContent = _contentController.text;
                        _koTitle =
                            await Translate('vi', _viTitle).translateViToKo();
                        _koContent =
                            await Translate('vi', _viContent).translateViToKo();
                        _enTitle =
                            await Translate('ko', _koTitle).translateKoToEn();
                        _enContent =
                            await Translate('ko', _koContent).translateKoToEn();
                      }

                      Firestore.instance
                          .collection('GroupBoard')
                          .document(board.bid)
                          .updateData({
                        'title': [_koTitle, _enTitle, _viTitle],
                        'content': [_koContent, _enContent, _viContent],
                        'image':
                            _imageURLend != null ? _imageURLend : board.image,
                        'owner': user.uid,
                        //create , modify 추가
                      });
                    }
                  },
                  child: AutoSizeText(
                    user.language == "ko" ? "수정":
                    user.language == "en" ?
                    "Edit" : "Biên tập",
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 18,
                        fontWeight: FontWeight.w600),
                    textAlign: TextAlign.center,
                    maxLines: 1,
                  ),
                ),
              )
            ],
          ),
          body: ListView(
              padding: EdgeInsets.symmetric(horizontal: 24.0),
              children: <Widget>[
                SizedBox(
                  height: 30,
                ),
                Add()
              ]),
        )));
  }

  Widget Add() {
    return Form(
        key: _formKey,
        child: Column(children: <Widget>[
          TextFormField(
            controller: _titleController,
            validator: (value) {
              return value.isEmpty ? 'Enter Title' : null;
            },
          ),
          TextFormField(
            controller: _contentController,
            validator: (value) {
              return value.isEmpty ? 'Enter Contents' : null;
            },
            maxLines: 16,
          ),
          SizedBox(
            height: 10,
          ),
          Row(children: <Widget>[
            Container(
              alignment: Alignment.centerLeft,
              width: 100,
              height: 60,
              child: FittedBox(
                fit: BoxFit.fill,
                child: FutureBuilder(
                    future: _imageURL,
                    builder:
                        (BuildContext context, AsyncSnapshot<String> snapshot) {
                      if (snapshot.hasError)
                        return LinearProgressIndicator(
                          valueColor:
                              new AlwaysStoppedAnimation<Color>(Colors.blue),
                        );
                      if (!snapshot.hasData) {
                        if (board.image != null)
                          return Image.network(widget.board.image);
                        else
                          return Image.asset("assets/logo.png",
                              color: Colors.black);
                      }
                      if (snapshot.data == null) if (board.image != null)
                        return Image.network(widget.board.image);
                      else
                        return Image.asset("assets/logo.png",
                            color: Colors.black);
                      else {
                        _imageURLend = snapshot.data;
                        return Image.network(_imageURLend);
                      }
                    }),
              ),
            ),
            SizedBox(width: 100),
            IconButton(
                icon: Icon(Icons.camera_alt),
                iconSize: 40,
                onPressed: () async {
                  if (user.language == "ko") {
                    _koTitle = _titleController.text;
                    _koContent = _contentController.text;
                    _enTitle =
                        await Translate('ko', _koTitle).translateKoToEn();
                    _enContent =
                        await Translate('ko', _koContent).translateKoToEn();
                    _viTitle =
                        await Translate('ko', _koTitle).translateKoToVi();
                    _viContent =
                        await Translate('ko', _koContent).translateKoToVi();
                  } else if (user.language == 'en') {
                    _enTitle = _titleController.text;
                    _enContent = _contentController.text;
                    _koTitle =
                        await Translate('en', _enTitle).translateEnToKo();
                    _koContent =
                        await Translate('en', _enContent).translateEnToKo();
                    _viTitle =
                        await Translate('ko', _koTitle).translateKoToVi();
                    _viContent =
                        await Translate('ko', _koContent).translateKoToVi();
                  } else {
                    _viTitle = _titleController.text;
                    _viContent = _contentController.text;
                    _koTitle =
                        await Translate('vi', _viTitle).translateViToKo();
                    _koContent =
                        await Translate('vi', _viContent).translateViToKo();
                    _enTitle =
                        await Translate('ko', _koTitle).translateKoToEn();
                    _enContent =
                        await Translate('ko', _koContent).translateKoToEn();
                  }

                  Firestore.instance
                      .collection('GroupBoard')
                      .document(board.bid)
                      .updateData({
                    'title': [_koTitle, _enTitle, _viTitle],
                    'content': [_koContent, _enContent, _viContent],
                    //create , modify 추가
                  });
                  getImageFromCam();
                }),
            IconButton(
                alignment: Alignment.centerRight,
                icon: Icon(Icons.album),
                iconSize: 40,
                onPressed: () async {
                  if (user.language == "ko") {
                    _koTitle = _titleController.text;
                    _koContent = _contentController.text;
                    _enTitle =
                        await Translate('ko', _koTitle).translateKoToEn();
                    _enContent =
                        await Translate('ko', _koContent).translateKoToEn();
                    _viTitle =
                        await Translate('ko', _koTitle).translateKoToVi();
                    _viContent =
                        await Translate('ko', _koContent).translateKoToVi();
                  } else if (user.language == 'en') {
                    _enTitle = _titleController.text;
                    _enContent = _contentController.text;
                    _koTitle =
                        await Translate('en', _enTitle).translateEnToKo();
                    _koContent =
                        await Translate('en', _enContent).translateEnToKo();
                    _viTitle =
                        await Translate('ko', _koTitle).translateKoToVi();
                    _viContent =
                        await Translate('ko', _koContent).translateKoToVi();
                  } else {
                    _viTitle = _titleController.text;
                    _viContent = _contentController.text;
                    _koTitle =
                        await Translate('vi', _viTitle).translateViToKo();
                    _koContent =
                        await Translate('vi', _viContent).translateViToKo();
                    _enTitle =
                        await Translate('ko', _koTitle).translateKoToEn();
                    _enContent =
                        await Translate('ko', _koContent).translateKoToEn();
                  }
                  Firestore.instance
                      .collection('GroupBoard')
                      .document(board.bid)
                      .updateData({
                    'title': [_koTitle, _enTitle, _viTitle],
                    'content': [_koContent, _enContent, _viContent],
                    //create , modify 추가
                  });
                  getImageFromGallery();
                })
          ])
        ]));
  }

  Future getImageFromCam() async {
    final image = await ImagePicker().getImage(source: ImageSource.camera);

    setState(() {
      _image = File(image.path);
    });
  }

  Future getImageFromGallery() async {
    final image = await ImagePicker().getImage(source: ImageSource.gallery);

    setState(() {
      _image = File(image.path);
      _imageURL = uploadImageFile(_image);
    });
  }

  Future<String> uploadImageFile(File file) async {
    String temp = "";
    final String rand = "${new Random().nextInt(10000)}";
    final StorageReference ref =
        storage.ref().child('group_board').child('$rand.jpg');
    final StorageUploadTask uploadTask = ref.putFile(file);
    await (await uploadTask.onComplete)
        .ref
        .getDownloadURL()
        .then((dynamic url) {
      temp = url;
    });
    return temp;
  }
}

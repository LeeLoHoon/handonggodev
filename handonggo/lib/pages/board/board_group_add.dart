import 'dart:io';
import 'dart:math';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:handong_go/blocs/board_bloc/board_state.dart';
import 'package:handong_go/models/user_model.dart';
import 'package:handong_go/utils/colors.dart';
import 'package:handong_go/utils/translate.dart';
import 'package:handong_go/utils/user_util.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';

class GroupBoardAdd extends StatefulWidget {
  final BoardState state;
  GroupBoardAdd(this.state);
  @override
  GroupBoardAddState createState() {
    return GroupBoardAddState(state: state);
  }
}

class GroupBoardAddState extends State<GroupBoardAdd> {
// TODO: Add text editing controllers (101)
  final BoardState state;
  GroupBoardAddState({this.state});

  User user = UserUtil.getUser();
  String pw;
  String time;
  bool check;
  final _formKey = GlobalKey<FormState>();
  File _image;
  Future<String> _imageURL;
  String _imageURLend;
  String _title;
  String _content;
  FirebaseApp app;
  FirebaseStorage storage =
      new FirebaseStorage(storageBucket: 'gs://handonggo-b3d60.appspot.com/');

  String _koTitle, _koContent;
  String _enTitle, _enContent;
  String _viTitle, _viContent;

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () async => false,
        child: MaterialApp(
            home: Scaffold(
          appBar: AppBar(
            leading: new IconButton(
              icon: BackButton(
                color: Colors.white,
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
            ),
            backgroundColor: mainColor,
            automaticallyImplyLeading: false,
            elevation: 0,
            title: Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Container(
                    width: MediaQuery.of(context).size.width * 0.6,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        AutoSizeText(
                          user.language == "ko" ? "그룹 글쓰기":
                          user.language == "en" ?
                          "Group Board" : "bài báo tập thể",
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 18,
                              fontWeight: FontWeight.w600),
                          textAlign: TextAlign.center,
                          maxLines: 1,
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            actions: <Widget>[
              Container(
                margin: const EdgeInsets.all(10.0),
                alignment: Alignment(-3, 0),
                child: GestureDetector(
                  onTap: () async {
                    if (_formKey.currentState.validate()) {
                      Navigator.pop(context);
                      DateTime now = DateTime.now();
                      String formattedDate =
                          DateFormat("yyyy.MM.dd.HH.MM.SS").format(now);

                      if (user.language == "ko") {
                        _koTitle = _title;
                        _koContent = _content;
                        _enTitle =
                            await Translate('ko', _koTitle).translateKoToEn();
                        _enContent =
                            await Translate('ko', _koContent).translateKoToEn();
                        _viTitle =
                            await Translate('ko', _koTitle).translateKoToVi();
                        _viContent =
                            await Translate('ko', _koContent).translateKoToVi();
                      } else if (user.language == 'en') {
                        _enTitle = _title;
                        _enContent = _content;
                        _koTitle =
                            await Translate('en', _enTitle).translateEnToKo();
                        _koContent =
                            await Translate('en', _enContent).translateEnToKo();
                        _viTitle =
                            await Translate('ko', _koTitle).translateKoToVi();
                        _viContent =
                            await Translate('ko', _koContent).translateKoToVi();
                      } else {
                        _viTitle = _title;
                        _viContent = _content;
                        _koTitle =
                            await Translate('vi', _viTitle).translateViToKo();
                        _koContent =
                            await Translate('vi', _viContent).translateViToKo();
                        _enTitle =
                            await Translate('ko', _koTitle).translateKoToEn();
                        _enContent =
                            await Translate('ko', _koContent).translateKoToEn();
                      }

                      Firestore.instance
                          .collection('GroupBoard')
                          .document(
                              '${user.email.substring(0, 8)}$formattedDate')
                          .setData({
                        'bid': '${user.email.substring(0, 8)}$formattedDate',
                        'name': user.name,
                        'title': [_koTitle, _enTitle, _viTitle],
                        'content': [_koContent, _enContent, _viContent],
                        'image': _imageURLend,
                        'likeCnt': 0,
                        'likers': [],
                        'koComments': [],
                        'enComments': [],
                        'viComments': [],
                        'owner': user.uid,
                        'date': formattedDate,
                        'theme': "group"
                        //create , modify 추가
                      });
                    }
                  },
                  child: AutoSizeText(
                    user.language == "ko" ? "작성":
                    user.language == "en" ?
                    "Add" : "sự viết lách",
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 18,
                        fontWeight: FontWeight.w600),
                    textAlign: TextAlign.center,
                    maxLines: 1,
                  ),
                ),
              )
            ],
          ),
          body: ListView(
              padding: EdgeInsets.symmetric(horizontal: 24.0),
              children: <Widget>[
                SizedBox(
                  height: 30,
                ),
                Add(state)
              ]),
        )));
  }

  Widget Add(BoardState state) {
    return Form(
        key: _formKey,
        child: Column(children: <Widget>[
          TextFormField(
            validator: (value) {
              return value.isEmpty ? 'Enter Title' : null;
            },
            onChanged: (text) {
              _title = text;
            },
            decoration: InputDecoration(
                hintText: user.language == "ko" ? "제목을 입력하세요.":
                user.language == "en" ?
                "Enter a title" : "Hãy nhập tựa đề",
                hintStyle: TextStyle(fontSize: 20, color: Colors.black)),
          ),
          TextFormField(
            validator: (value) {
              return value.isEmpty ? 'Enter Contents' : null;
            },
            onChanged: (text) {
              _content = text;
            },
            maxLines: 16,
            decoration: InputDecoration(
              alignLabelWithHint: true,
              hintText: user.language == "ko" ? "내용을 입력하세요.":
              user.language == "en" ?
              "Enter the contents" : "Xin vui lòng nhập nội dung",
              hintStyle: TextStyle(fontSize: 15, color: Colors.grey),
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Row(children: <Widget>[
            Container(
              alignment: Alignment.centerLeft,
              width: 100,
              height: 60,
              child: FittedBox(
                fit: BoxFit.fill,
                child: FutureBuilder(
                    future: _imageURL,
                    builder:
                        (BuildContext context, AsyncSnapshot<String> snapshot) {
                      if (snapshot.hasError)
                        return LinearProgressIndicator(
                          valueColor:
                              new AlwaysStoppedAnimation<Color>(Colors.blue),
                        );
                      if (!snapshot.hasData) {
                        return Image.asset(
                          "assets/logo.png",
                          color: Colors.black,
                        );
                      }
                      if (snapshot.data == null)
                        return Image.asset("assets/logo.png",
                            color: Colors.red);
                      else {
                        _imageURLend = snapshot.data;
                        return Image.network(_imageURLend);
                      }
                    }),
              ),
            ),
            SizedBox(width: 100),
            IconButton(
                icon: Icon(Icons.camera_alt),
                iconSize: 40,
                onPressed: () {
                  getImageFromCam();
                }),
            IconButton(
                alignment: Alignment.centerRight,
                icon: Icon(Icons.album),
                iconSize: 40,
                onPressed: () {
                  getImageFromGallery();
                })
          ])
        ]));
  }

  Future getImageFromCam() async {
    final image = await ImagePicker().getImage(source: ImageSource.camera);
    setState(() {
      _image = File(image.path);
    });
  }

  Future getImageFromGallery() async {
    final image = await ImagePicker().getImage(source: ImageSource.gallery);
    setState(() {
      _image = File(image.path);
      _imageURL = uploadImageFile(_image);
    });
  }

  Future<String> uploadImageFile(File file) async {
    String temp = "";
    final String rand = "${new Random().nextInt(10000)}";
    final StorageReference ref =
        storage.ref().child('group_board').child('$rand.jpg');
    final StorageUploadTask uploadTask = ref.putFile(file);
    await (await uploadTask.onComplete)
        .ref
        .getDownloadURL()
        .then((dynamic url) {
      temp = url;
    });
    return temp;
  }
}

import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:handong_go/utils/colors.dart';
import 'package:handong_go/widgets/chat_room_item.dart';
import 'package:handong_go/widgets/column.builder.dart';
import 'package:handong_go/widgets/handong_go_appbar.dart';
import 'package:handong_go/widgets/hgo_round_btn.dart';
import 'package:handong_go/widgets/search_bar.dart';
import 'package:rxdart/rxdart.dart';

import 'make_room.dart';

class Chatting extends StatefulWidget {
  @override
  _ChattingState createState() => _ChattingState();
}

class _ChattingState extends State<Chatting> {
  BehaviorSubject<List<DocumentSnapshot>> _roomsController =
  BehaviorSubject<List<DocumentSnapshot>>();
  Stream<List<DocumentSnapshot>> get rooms => _roomsController;
  TextEditingController _searchQuery = new TextEditingController();
  Timer _debounce;

  @override
  void initState() {
    super.initState();
    _searchQuery.addListener(_debouncedSearch);
    _debouncedSearch();
    getChatList();
  }

  @override
  void dispose() {
    _searchQuery.dispose();
    _roomsController.close();
    super.dispose();
  }

  void _debouncedSearch() {
    if (_debounce?.isActive ?? false) _debounce.cancel();
    _debounce = Timer(const Duration(milliseconds: 500), () {
      getChatList();
    });
  }

  void getChatList() {
    List<DocumentSnapshot> _docs = [];

    Firestore.instance
        .collection('ChatRooms')
        .orderBy("lastAdded", descending: true)
        .where("keywords", arrayContains: _searchQuery.text)
        .getDocuments()
        .then((QuerySnapshot qs) {
      qs.documents.forEach((DocumentSnapshot ds) {
        _docs.add(ds);
      });
      _roomsController.sink.add(_docs);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: HgoAppBar(

      ),
      body: Column(
        children: <Widget>[

          HgoSearchBar(
            controller: _searchQuery,
            hintText: "채팅방을 검색해보세요",
          ),
          Expanded(
            child: ListView(
              children: <Widget>[
                StreamBuilder<List<DocumentSnapshot>>(
                  stream: rooms,
                  builder: (context, snapshot) {
                    if (!snapshot.hasData) return Container();
                    return ColumnBuilder(
                        itemCount: snapshot.data.length,
                        itemBuilder: (BuildContext context, int index) {
                          return StreamBuilder<QuerySnapshot>(
                              stream: Firestore.instance
                                  .collection('RoomMember')
                                  .where("roomNo",
                                  isEqualTo:
                                  snapshot.data[index].data['roomNo'])
                                  .snapshots(),
                              builder: (context, memberSnapshot) {
                                if (!memberSnapshot.hasData) return Container();
                                return ChatRoomItem(
                                  ds: snapshot.data[index],
                                  memberSnapshot: memberSnapshot,
                                  context: context,
                                  refresh: getChatList,
                                );
                              });
                        });
                  },
                ),
              ],
            ),
          ),

          Center(
            child: InkWell(
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (BuildContext context) =>
                              MakeRoomPage())).then((void v) => getChatList());
                },
                child: HgoRoundBtn(
                    textColor: Colors.white,
                    backgroundColor: mainColor,
                    text: "방만들기")),
          ),
        ],
      ),
    );
  }
}

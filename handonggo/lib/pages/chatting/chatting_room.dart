import 'dart:async';
import 'dart:convert';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:handong_go/utils/LocaleUtil.dart';
import 'package:handong_go/utils/colors.dart';
import 'package:handong_go/utils/commons.dart';
import 'package:handong_go/utils/date_util.dart';
import 'package:handong_go/utils/user_util.dart';
import 'package:handong_go/widgets/column.builder.dart';
import 'package:http/http.dart' as http;


class ChatRoomPage extends StatefulWidget {
  static final String route = "/chatroom";
  final String roomId;
  final String roomNm;
  final int peopleMax;
  final int peopleCnt;
  final String account;

  ChatRoomPage({
    this.roomId,
    this.roomNm,
    this.peopleMax,
    this.peopleCnt,
    this.account,
  });

  @override
  _ChatRoomPageState createState() => _ChatRoomPageState();
}

class _ChatRoomPageState extends State<ChatRoomPage> {
  ScrollController _scrollController = new ScrollController();
  TextEditingController _textEditingController = TextEditingController();
  String _locale = LocaleUtil.getLocale();

  Future<String> translateLan(String originReview, String country) async {
    var response = await http.post(
        Uri.encodeFull("https://openapi.naver.com/v1/papago/n2mt"),
        headers: {
          "Content-Type": "application/x-www-form-urlencoded; charset=UTF-8",
          "X-Naver-Client-Id": "7CkaJUA8kNNsUsZyOWnG",
          "X-Naver-Client-Secret": "235Zlt2J2C",
        },
        body: country == "ko"
            ? "source=ko&target=en&text=$originReview"
            : "source=en&target=ko&text=$originReview");
    print(json.decode(response.body)["message"]["result"]["translatedText"]);

    return json.decode(response.body)["message"]["result"]["translatedText"];
  }

  Future<String> languageKnow(String originReview) async {
    var response = await http.post(
        Uri.encodeFull("https://openapi.naver.com/v1/papago/detectLangs"),
        headers: {
          "Content-Type": "application/x-www-form-urlencoded; charset=UTF-8",
          "X-Naver-Client-Id": "7CkaJUA8kNNsUsZyOWnG",
          "X-Naver-Client-Secret": "235Zlt2J2C",
        },
        body: "query=$originReview");
    print(json.decode(response.body)["langCode"]);

    return json.decode(response.body)["langCode"];
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: mainColor,
        automaticallyImplyLeading: false,
        elevation: 0,
        title: Container(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              BackButton(
                color: Colors.white,
              ),
              StreamBuilder<QuerySnapshot>(
                  stream: Firestore.instance
                      .collection('RoomMember')
                      .where("roomNo", isEqualTo: widget.roomId)
                      .snapshots(),
                  builder: (BuildContext context,
                      AsyncSnapshot<QuerySnapshot> snapshot) {
                    if (!snapshot.hasData) return Container();
                    return Container(
                      width: MediaQuery.of(context).size.width * 0.6,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          AutoSizeText(
                            widget.roomNm,
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 18,
                                fontWeight: FontWeight.w600),
                            textAlign: TextAlign.center,
                            maxLines: 1,
                          ),
                          SizedBox(width: 5),
                          Text(
                            "(${snapshot.data.documents.length.toString()}/${widget.peopleMax.toString()})",
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 16,
                                fontWeight: FontWeight.w600),
                            textAlign: TextAlign.center,
                            maxLines: 1,
                          ),
                        ],
                      ),
                    );
                  }),
              IconButton(
                icon: Icon(Icons.exit_to_app),
                color: Colors.white,
                onPressed: () async {
                  try {
                    WriteBatch batch = Firestore.instance.batch();
                    batch.delete(Firestore.instance.document(
                        '/RoomMember/${UserUtil.getUser().uid + widget.roomId}'));
                    batch.setData(
                        Firestore.instance
                            .collection('ChatRooms')
                            .document(widget.roomId),
                        {'${UserUtil.getUser().uid}': false},
                        merge: true);
                    await batch.commit();
                    Navigator.pop(context);
                  } catch (e) {
                    print(e);
                  }
                },
              )
            ],
          ),
        ),
      ),
      body: StreamBuilder<QuerySnapshot>(
        stream: Firestore.instance
            .collection('ChatRooms')
            .document(widget.roomId)
            .collection('Chats')
            .snapshots(),
        builder: (context, snapshot) {
          if (!snapshot.hasData) return Container();
          return Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 16.0),
                  child: ListView(
                    controller: _scrollController,
                    reverse: true,
                    shrinkWrap: true,
                    children: [
                      ColumnBuilder(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          itemCount: snapshot.data.documents.length,
                          itemBuilder: (BuildContext context, int index) {
                            return Bubble(
                              message: _locale == "ko"
                                  ? snapshot
                                  .data.documents[index].data['koMessage']
                                  : snapshot
                                  .data.documents[index].data['enMessage'],
                              time: DateTimeUtil.dateSub(snapshot
                                  .data.documents[index].data['id']) ??
                                  "",
                              delivered: true,
                              isMe: snapshot
                                  .data.documents[index].data['userId'] ==
                                  UserUtil.getUser().uid,
                              userNm:
                              snapshot.data.documents[index].data['userNm'],
                              profileUrl: snapshot
                                  .data.documents[index].data['profileUrl'],
                            );
                          })
                    ],
                  ),
                ),
              ),
              SizedBox(height: 5),
              chatTypeBox()
            ],
          );
        },
      ),
    );
  }

  Widget chatTypeBox() {
    return Container(
      color: Colors.white,
      height: 65,
      alignment: Alignment.center,
      child: Container(
        width: MediaQuery.of(context).size.width * 0.9,
        height: 45,
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.all(Radius.circular(10))),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Expanded(
              child: TextField(
                controller: _textEditingController,
                maxLines: 1,
                decoration: InputDecoration(
                    contentPadding: EdgeInsets.all(8),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10),
                        borderSide: BorderSide.none)),
              ),
            ),
            Container(
              width: 2,
              height: 35,
              color: Colors.white,
              margin: EdgeInsets.symmetric(horizontal: 3),
            ),
            Padding(
              padding: const EdgeInsets.all(4.0),
              child: InkWell(
                onTap: () {
                  sendChat();
                },
                child: Container(
                  decoration: BoxDecoration(
                      color: mainColor,
                      borderRadius: BorderRadius.circular(10)),
                  width: 60,
                  height: 45,
                  child: Icon(
                    Icons.keyboard_arrow_down,
                    color: Colors.white,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Future sendChat() async {
    String id = Timestamp.now().microsecondsSinceEpoch.toString();
    String text = _textEditingController.text;

    String country = await languageKnow(text);
    String translateMessage = await translateLan(text, country);
    print("============================");
    print(translateMessage);

    String koMessage;
    String enMessage;
    if (country == "ko") {
      koMessage = text;
      enMessage = translateMessage;
    } else {
      enMessage = text;
      koMessage = translateMessage;
    }
    Firestore.instance
        .document('/ChatRooms/${widget.roomId}/Chats/$id')
        .setData({
      'userId': UserUtil.getUser().uid,
      'userNm': UserUtil.getUser().name,
      'profileUrl': UserUtil.getUser().url,
      'message': text,
      'enMessage': enMessage,
      'koMessage': koMessage,
      'id': id
    }).then((onValue) {
      _textEditingController.clear();
      _scrollController.animateTo(
        0.0,
        curve: Curves.easeOut,
        duration: const Duration(milliseconds: 300),
      );
    });
    print('어??');
    Firestore.instance
        .document('/ChatRooms/${widget.roomId}')
        .setData({'lastMsg': text, 'lastAdded': id}, merge: true);
  }
}

class Bubble extends StatelessWidget {
  Bubble(
      {this.message,
        this.time,
        this.delivered,
        this.isMe,
        this.userNm,
        this.profileUrl});

  final String message, time, userNm, profileUrl;
  final delivered, isMe;

  @override
  Widget build(BuildContext context) {
    final bg = !isMe ? Colors.white : subColor;
    final align = !isMe ? MainAxisAlignment.start : MainAxisAlignment.end;
    final radius = BorderRadius.all(Radius.circular(15.0));

    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8.0),
      child: Row(
        mainAxisAlignment: align,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          !isMe
              ? profileUrl != null
              ? CircleAvatar(
            radius: 23,
            backgroundImage: CachedNetworkImageProvider(profileUrl),
          )
              : CircleAvatar(
            radius: 23,
            backgroundImage: CachedNetworkImageProvider(guestImg),
          )
              : Container(),
          Padding(
            padding: const EdgeInsets.only(left: 10),
            child: !isMe
                ? Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                !isMe
                    ? Text(
                  userNm,
                  style: TextStyle(
                      fontSize: 14,
                      color: Colors.white,
                      fontWeight: FontWeight.w600),
                )
                    : Container(),
                Container(
                  constraints: BoxConstraints(
                      maxWidth: MediaQuery.of(context).size.width * 0.6),
                  padding: const EdgeInsets.symmetric(
                      vertical: 10.0, horizontal: 15),
                  margin: EdgeInsets.only(top: 5),
                  decoration: BoxDecoration(
                    color: bg,
                    borderRadius: radius,
                  ),
                  child: AutoSizeText(
                    message ?? "",
                    style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.w500),
                  ),
                ),
              ],
            )
                : Container(
              constraints: BoxConstraints(
                  maxWidth: MediaQuery.of(context).size.width * 0.6),
              padding: const EdgeInsets.symmetric(
                  vertical: 10.0, horizontal: 15),
              decoration: BoxDecoration(
                color: mainColor,
                borderRadius: radius,
              ),
              child: AutoSizeText(
                message ?? "",
                style: TextStyle(color: Colors.white),
              ),
            ),
          )
        ],
      ),
    );
  }
}

import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:handong_go/utils/colors.dart';
import 'package:handong_go/utils/commons.dart';
import 'package:handong_go/utils/user_util.dart';
import 'package:handong_go/widgets/handong_go_appbar.dart';
import 'package:handong_go/widgets/hgo_round_btn.dart';
import 'package:http/http.dart' as http;
import 'package:image_picker/image_picker.dart';

class MakeRoomPage extends StatefulWidget {
  @override
  _MakeRoomPageState createState() => _MakeRoomPageState();
}

class _MakeRoomPageState extends State<MakeRoomPage> {
  final _nameController = TextEditingController();
  final _descController = TextEditingController();
  final _formKey = GlobalKey<FormState>();

  FirebaseStorage _storage = FirebaseStorage.instance;
  List<DropdownMenuItem> options = [];

  File _image;
  int peopleMax = 2;

  String roomNm;
  String phoneNumber;
  String account;
  String roomNo = DateTime.now().microsecondsSinceEpoch.toString();
  int peopleCnt = 1;
  String chatRoomImage = "";

  Future<String> translateLan(String originReview, String country) async {
    var response = await http.post(
        Uri.encodeFull("https://openapi.naver.com/v1/papago/n2mt"),
        headers: {
          "Content-Type": "application/x-www-form-urlencoded; charset=UTF-8",
          "X-Naver-Client-Id": "7CkaJUA8kNNsUsZyOWnG",
          "X-Naver-Client-Secret": "235Zlt2J2C",
        },
        body: country == "ko"
            ? "source=ko&target=en&text=$originReview"
            : "source=en&target=ko&text=$originReview");
    print(response.body);
    print(json.decode(response.body)["message"]["result"]["translatedText"]);

    return json.decode(response.body)["message"]["result"]["translatedText"];
  }

  Future<String> languageKnow(String originReview) async {
    var response = await http.post(
        Uri.encodeFull("https://openapi.naver.com/v1/papago/detectLangs"),
        headers: {
          "Content-Type": "application/x-www-form-urlencoded; charset=UTF-8",
          "X-Naver-Client-Id": "7CkaJUA8kNNsUsZyOWnG",
          "X-Naver-Client-Secret": "235Zlt2J2C",
        },
        body: "query=$originReview");
    print(json.decode(response.body)["langCode"]);

    return json.decode(response.body)["langCode"];
  }

  @override
  void initState() {
    super.initState();
    options = List.generate(
        8,
            (int index) => DropdownMenuItem(
          value: index + 2,
          child: Text(
            '${index + 2}',
            style: TextStyle(color: Colors.black),
          ),
        ));
  }

  Future saveToDatabase() async {
    String country = await languageKnow(_nameController.text);
    String translateName = await translateLan(_nameController.text, country);
    String translateDesc = await translateLan(_descController.text, country);
    String koName;
    String enName;
    String koDesc;
    String enDesc;

    List<String> ar = [""];

    if (country == "ko") {
      koName = _nameController.text;
      enName = translateName;
      koDesc = _descController.text;
      enDesc = translateDesc;
    } else {
      enName = _nameController.text;
      koName = translateName;
      enDesc = _descController.text;
      koDesc = translateDesc;
    }

    for (var i = 0; i < koName.length; i++) {
      for (var j = i; j < koName.length; j++) {
        ar.add(koName.substring(i, j + 1));
      }
    }
    for (var i = 0; i < enName.length; i++) {
      for (var j = i; j < enName.length; j++) {
        ar.add(enName.substring(i, j + 1));
      }
    }
    for (var i = 0; i < koDesc.length; i++) {
      for (var j = i; j < koDesc.length; j++) {
        ar.add(koDesc.substring(i, j + 1));
      }
    }
    for (var i = 0; i < enDesc.length; i++) {
      for (var j = i; j < enDesc.length; j++) {
        ar.add(enDesc.substring(i, j + 1));
      }
    }

    var data = {
      "roomNm": _nameController.text,
      "desc": _descController.text,
      "roomNo": roomNo,
      "lastAdded": roomNo,
      "peopleMax": peopleMax,
      "peopleCnt": peopleCnt,
      "chatRoomImage": chatRoomImage,
      "leader": UserUtil.getUser().uid,
      "keywords": ar,
      "koName": koName,
      "enName": enName,
      "koDesc": koDesc,
      "enDesc": enDesc,
      "${UserUtil.getUser().uid}": true
    };

    WriteBatch batch = Firestore.instance.batch();
    batch.setData(
        Firestore.instance.collection('ChatRooms').document(roomNo), data,
        merge: true);
    batch.setData(
        Firestore.instance
            .collection('RoomMember')
            .document(UserUtil.getUser().uid + roomNo),
        {'uid': UserUtil.getUser().uid, 'roomNo': roomNo},
        merge: true);
    await batch.commit();
  }

  Future getImage(ImageSource source) async {
    _image = await ImagePicker.pickImage(source: source);
  }

  Future _uploadPic(BuildContext context) async {
    var now = DateTime.now();
    String fileName = now.millisecondsSinceEpoch.toString();
    StorageReference reference = _storage.ref().child(fileName);
    print(_storage.toString());
    StorageUploadTask uploadTask = reference.putFile(_image);
    StorageTaskSnapshot taskSnapshot = await uploadTask.onComplete;
    chatRoomImage = await taskSnapshot.ref.getDownloadURL();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: HgoAppBar(

      ),
      backgroundColor: Colors.white,
      body: Padding(
        padding: const EdgeInsets.fromLTRB(15, 10, 15, 10),
        child: Form(
          key: _formKey,
          child: ListView(
            children: <Widget>[
              SizedBox(height: 30),
              chooseChatImage(),
              SizedBox(height: 30),
              Column(
                children: <Widget>[
                  roomNameForm(),
                  Divider(
                    height: 30,
                  ),
                  peopleDropdown(),
                  SizedBox(height: 20),
                  descForm(),
                ],
              ),
              SizedBox(
                height: 30,
              ),
            ],
          ),
        ),
      ),
      bottomNavigationBar: InkWell(
          onTap: () async {
            if (_formKey.currentState.validate()) {
              _image != null ?
              await _uploadPic(context) : print("no image");
              saveToDatabase();
              Navigator.of(context).pop();
            }
          },
          child: Container(
            height: 80,
            child: HgoRoundBtn(
                textColor: Colors.white,
                backgroundColor:
                _formKey.currentState != null &&
                    _formKey.currentState.validate()
                    ? mainColor
                    : subColor,
                text: "방 만들기"),
          )),
    );
  }

  Widget chooseChatImage() {
    return Column(
      children: <Widget>[
        Stack(
          children: <Widget>[
            Container(
                width: MediaQuery.of(context).size.width / 2.5,
                height: MediaQuery.of(context).size.width / 2.5,
                decoration: new BoxDecoration(
                    shape: BoxShape.circle,
                    image: new DecorationImage(
                        fit: BoxFit.cover,
                        image: _image == null
                            ? CachedNetworkImageProvider(guestImg)
                            : FileImage(_image)))),
            Positioned(
              bottom: 0,
              right: 10,
              child: IconButton(
                icon: Icon(
                  Icons.add_circle,
                  color: mainColor,
                  size: 40,
                ),
                onPressed: () {
                  getImage(ImageSource.gallery);
                },
              ),
            ),
          ],
        ),
      ],
    );
  }

  Widget roomNameForm() {
    return Row(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.fromLTRB(0, 0, 10, 0),
          child: Text(
            "방 이름",
            style: TextStyle(fontSize: MediaQuery.of(context).size.height / 50),
          ),
        ),
        Expanded(
          child: TextFormField(
            validator: (value) {
              if (value.isEmpty) {
                return 'Please enter some text';
              }
              return null;
            },
            decoration: InputDecoration(
                contentPadding: new EdgeInsets.symmetric(
                    vertical: MediaQuery.of(context).size.height / 80,
                    horizontal: 10.0),
                fillColor: Colors.white,
                filled: true,
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10),
                    borderSide: BorderSide.none)),
            controller: _nameController,
          ),
        ),
      ],
    );
  }

  Widget peopleDropdown() {
    return Row(
      children: <Widget>[
        Container(
          width: 80,
          child: Text(
            "사람 수",
            style: TextStyle(fontSize: MediaQuery.of(context).size.height / 50),
          ),
        ),
        Container(
          width: 70,
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(10),
          ),
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10.0),
            child: DropdownButton(
              items: options,
              onChanged: (value) {
                setState(() {
                  peopleMax = value;
                });
              },
              value: peopleMax,
              style: TextStyle(fontSize: 16),
              isExpanded: true,
              underline: Container(),
            ),
          ),
        ),
      ],
    );
  }

  Widget descForm() {
    return Row(
      children: <Widget>[
        Container(
          width: 80,
          child: Text(
            "설명",
            style: TextStyle(fontSize: MediaQuery.of(context).size.height / 50),
          ),
        ),
        Expanded(
          child: TextFormField(
            validator: (value) {
              if (value.isEmpty) {
                return 'Please enter some text';
              }
              return null;
            },
            decoration: InputDecoration(
                contentPadding: new EdgeInsets.symmetric(
                    vertical: MediaQuery.of(context).size.height / 80,
                    horizontal: 10.0),
                fillColor: Colors.white,
                filled: true,
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10),
                    borderSide: BorderSide.none)),
            controller: _descController,
          ),
        ),
      ],
    );
  }
}

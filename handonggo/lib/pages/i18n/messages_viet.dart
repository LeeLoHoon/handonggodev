// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a viet locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'viet';

  final messages = _notInlinedMessages(_notInlinedMessages);
  static _notInlinedMessages(_) => <String, Function> {
    "add" : MessageLookupByLibrary.simpleMessage("sự viết lách"),
    "appName" : MessageLookupByLibrary.simpleMessage("handonggo viet version"),
    "bad" : MessageLookupByLibrary.simpleMessage("không tốt"),
    "bestcontent" : MessageLookupByLibrary.simpleMessage("Bài dăng được yêu thích"),
    "description" : MessageLookupByLibrary.simpleMessage("sự miêu tả"),
    "enterContents" : MessageLookupByLibrary.simpleMessage("Xin vui lòng nhập nội dung"),
    "enterPrice" : MessageLookupByLibrary.simpleMessage("Hãy nhập giá"),
    "enterRestaurant" : MessageLookupByLibrary.simpleMessage("Hãy nhập tên nhà hàng."),
    "enterTitle" : MessageLookupByLibrary.simpleMessage("Hãy nhập tựa đề"),
    "food" : MessageLookupByLibrary.simpleMessage("thức ăn"),
    "free" : MessageLookupByLibrary.simpleMessage("Tự do"),
    "good" : MessageLookupByLibrary.simpleMessage("Thích quá"),
    "googleLogin" : MessageLookupByLibrary.simpleMessage("đăng nhập google"),
    "group" : MessageLookupByLibrary.simpleMessage("nhóm"),
    "logout" : MessageLookupByLibrary.simpleMessage("đăng xuất"),
    "makingRoom" : MessageLookupByLibrary.simpleMessage("Làm phòng"),
    "market" : MessageLookupByLibrary.simpleMessage("chợ"),
    "notBad" : MessageLookupByLibrary.simpleMessage("Không tệ"),
    "participatingChatRoom" : MessageLookupByLibrary.simpleMessage("Phòng chat có mặt"),
    "peopleNum" : MessageLookupByLibrary.simpleMessage("Số người"),
    "roomNm" : MessageLookupByLibrary.simpleMessage("Tên phòng"),
    "search" : MessageLookupByLibrary.simpleMessage("Tìm kiếm."),
    "searchTheChatRoom" : MessageLookupByLibrary.simpleMessage("Tìm kiếm phòng chat"),
    "veryGood" : MessageLookupByLibrary.simpleMessage("Rất tốt"),
    "widgetMessage" : MessageLookupByLibrary.simpleMessage("viet widget")
  };
}

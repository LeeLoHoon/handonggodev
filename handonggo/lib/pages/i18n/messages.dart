import 'package:intl/intl.dart';

class Messages {

  String get widgetMessage => Intl.message("",
      name: "widgetMessage"
  );

  String get appName => Intl.message("",
      name: "appName"
  );

  String get googleLogin => Intl.message("",
      name: "googleLogin"
  );

  String get  participatingChatRoom => Intl.message("",
      name: "participatingChatRoom"
  );

  String get bestcontent => Intl.message("",
      name: "bestcontent"
  );

  String get searchTheChatRoom => Intl.message("",
      name: "searchTheChatRoom"
  );

  String get makingRoom => Intl.message("",
      name: "makingRoom"
  );

  String get roomNm => Intl.message("",
      name: "roomNm"
  );

  String get peopleNum => Intl.message("",
      name: "peopleNum"
  );

  String get description => Intl.message("",
      name: "description"
  );

  String get search => Intl.message("",
      name: "search"
  );

  String get free => Intl.message("",
      name: "free"
  );

  String get food => Intl.message("",
      name: "food"
  );

  String get group => Intl.message("",
      name: "group"
  );

  String get market => Intl.message("",
      name: "market"
  );

  String get enterTitle => Intl.message("",
      name: "enterTitle"
  );

  String get enterContents => Intl.message("",
      name: "enterContents"
  );

  String get enterRestaurant => Intl.message("",
      name: "enterRestaurant"
  );

  String get enterPrice => Intl.message("",
      name: "enterPrice"
  );

  String get veryGood => Intl.message("",
      name: "veryGood"
  );

  String get good => Intl.message("",
      name: "good"
  );

  String get notBad => Intl.message("",
      name: "notBad"
  );

  String get bad => Intl.message("",
      name: "bad"
  );

  String get add => Intl.message("",
      name: "add"
  );

  String get logout => Intl.message("",
      name: "logout"
  );


}
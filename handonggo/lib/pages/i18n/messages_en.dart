// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a en locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'en';

  final messages = _notInlinedMessages(_notInlinedMessages);
  static _notInlinedMessages(_) => <String, Function> {
    "add" : MessageLookupByLibrary.simpleMessage("Add"),
    "appName" : MessageLookupByLibrary.simpleMessage("handongGo"),
    "bad" : MessageLookupByLibrary.simpleMessage("Bad"),
    "bestcontent" : MessageLookupByLibrary.simpleMessage("best content Top 5"),
    "description" : MessageLookupByLibrary.simpleMessage("description"),
    "enterContents" : MessageLookupByLibrary.simpleMessage("Enter the contents"),
    "enterPrice" : MessageLookupByLibrary.simpleMessage("Enter a price"),
    "enterRestaurant" : MessageLookupByLibrary.simpleMessage("Enter a restaurant."),
    "enterTitle" : MessageLookupByLibrary.simpleMessage("Enter a title"),
    "food" : MessageLookupByLibrary.simpleMessage("Food"),
    "free" : MessageLookupByLibrary.simpleMessage("Free"),
    "good" : MessageLookupByLibrary.simpleMessage("Good"),
    "googleLogin" : MessageLookupByLibrary.simpleMessage("google Login"),
    "group" : MessageLookupByLibrary.simpleMessage("Group"),
    "logout" : MessageLookupByLibrary.simpleMessage("Logout"),
    "makingRoom" : MessageLookupByLibrary.simpleMessage("Making room"),
    "market" : MessageLookupByLibrary.simpleMessage("Market"),
    "notBad" : MessageLookupByLibrary.simpleMessage("Not bad"),
    "participatingChatRoom" : MessageLookupByLibrary.simpleMessage("Participating Chat Room"),
    "peopleNum" : MessageLookupByLibrary.simpleMessage("Number of people"),
    "roomNm" : MessageLookupByLibrary.simpleMessage("Room name"),
    "search" : MessageLookupByLibrary.simpleMessage("Search"),
    "searchTheChatRoom" : MessageLookupByLibrary.simpleMessage("Search the chat room."),
    "veryGood" : MessageLookupByLibrary.simpleMessage("Very Good"),
    "widgetMessage" : MessageLookupByLibrary.simpleMessage("this is widget")
  };
}

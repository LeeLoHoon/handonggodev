// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a ko locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'ko';

  final messages = _notInlinedMessages(_notInlinedMessages);
  static _notInlinedMessages(_) => <String, Function> {
    "add" : MessageLookupByLibrary.simpleMessage("작성"),
    "appName" : MessageLookupByLibrary.simpleMessage("한동고"),
    "bad" : MessageLookupByLibrary.simpleMessage("나쁨"),
    "bestcontent" : MessageLookupByLibrary.simpleMessage("인기 게시물"),
    "description" : MessageLookupByLibrary.simpleMessage("설명"),
    "enterContents" : MessageLookupByLibrary.simpleMessage("내용을 입력하세요."),
    "enterPrice" : MessageLookupByLibrary.simpleMessage("가격을 입력하세요."),
    "enterRestaurant" : MessageLookupByLibrary.simpleMessage("식당 이름을 입력하세요."),
    "enterTitle" : MessageLookupByLibrary.simpleMessage("제목을 입력하세요."),
    "food" : MessageLookupByLibrary.simpleMessage("음식"),
    "free" : MessageLookupByLibrary.simpleMessage("자유"),
    "good" : MessageLookupByLibrary.simpleMessage("좋음"),
    "googleLogin" : MessageLookupByLibrary.simpleMessage("구글 로그인"),
    "group" : MessageLookupByLibrary.simpleMessage("그룹"),
    "logout" : MessageLookupByLibrary.simpleMessage("로그아웃"),
    "makingRoom" : MessageLookupByLibrary.simpleMessage("방만들기"),
    "market" : MessageLookupByLibrary.simpleMessage("장터"),
    "notBad" : MessageLookupByLibrary.simpleMessage("보통"),
    "participatingChatRoom" : MessageLookupByLibrary.simpleMessage("참여중인 채팅방"),
    "peopleNum" : MessageLookupByLibrary.simpleMessage("사람 수"),
    "roomNm" : MessageLookupByLibrary.simpleMessage("방 이름"),
    "search" : MessageLookupByLibrary.simpleMessage("게시물을 검색해보세요"),
    "searchTheChatRoom" : MessageLookupByLibrary.simpleMessage("채팅방을 검색해보세요"),
    "veryGood" : MessageLookupByLibrary.simpleMessage("매우 좋음"),
    "widgetMessage" : MessageLookupByLibrary.simpleMessage("위젯입니다.")
  };
}

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:handong_go/blocs/tab_bloc/bloc.dart';
import 'package:handong_go/utils/colors.dart';
import '../blocs/home_bloc/bloc.dart';
import 'board/board.dart';
import 'chatting/chatting.dart';
import 'home/home.dart';
import 'setting/setting.dart';

class BottomNaviBar extends StatefulWidget {
  const BottomNaviBar({Key key}) : super(key: key);

  @override
  _BottomNaviBarState createState() => _BottomNaviBarState();
}

class _BottomNaviBarState extends State<BottomNaviBar> {
  TabBloc _tabBloc = new TabBloc();
  PageController _pageController;
  HomeBloc _homeBloc;

  @override
  void initState() {
    super.initState();
    _pageController = new PageController(
      initialPage: 0,
    );
    _homeBloc = HomeBloc();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener(
        bloc: _tabBloc,
        listener: (BuildContext context, TabState state) {
          this._pageController.animateToPage(state.index,
              duration: const Duration(milliseconds: 500),
              curve: Curves.easeInOut);
        },
        child: BlocBuilder(
            bloc: _tabBloc,
            builder: (BuildContext context, TabState state) {
              return Scaffold(
                bottomNavigationBar: BottomNavigationBar(
                  backgroundColor: mainColor,
                  type: BottomNavigationBarType.fixed,
                  currentIndex: state.index,
                  items: <BottomNavigationBarItem>[
                    BottomNavigationBarItem(
                      icon: state.index == 0
                          ? Image.asset(
                        'assets/home2xxxhdpi.png',
                        width: 30,
                        height: 30,
                      )
                          : Image.asset(
                        'assets/homexxxhdpi.png',
                        width: 30,
                        height: 30,
                      ),
                      title: SizedBox(
                        height: 0,
                      ),
                    ),
                    BottomNavigationBarItem(
                      icon: state.index == 1
                          ? Image.asset(
                        'assets/chat2xxxhdpi.png',
                        width: 30,
                        height: 30,
                      )
                          : Image.asset(
                        'assets/chatxxxhdpi.png',
                        width: 30,
                        height: 30,
                      ),
                      title: SizedBox(
                        height: 0,
                      ),
                    ),
                    BottomNavigationBarItem(
                      icon: state.index == 2
                          ? Image.asset(
                        'assets/board2xxxhdpi.png',
                        width: 30,
                        height: 30,
                      )
                          : Image.asset(
                        'assets/boardxxxhdpi.png',
                        width: 30,
                        height: 30,
                      ),
                      title: SizedBox(
                        height: 0,
                      ),
                    ),
                    BottomNavigationBarItem(
                      icon: state.index == 3
                          ? Image.asset(
                        'assets/setting2xxxhdpi.png',
                        width: 30,
                        height: 30,
                      )
                          : Image.asset(
                        'assets/settingxxxhdpi.png',
                        width: 30,
                        height: 30,
                      ),
                      title: SizedBox(
                        height: 0,
                      ),
                    ),
                  ],
                  onTap: (index) {
                    _tabBloc.add(TabChange(index: index));
                  },
                ),
                body: PageView(
                  physics: NeverScrollableScrollPhysics(),
                  controller: _pageController,
                  onPageChanged: (index) {
                  },
                  children: <Widget>[
                    Home(),
                    Chatting(),
                    Board(),
                    Setting(),
                  ],
                ),
              );
            }));
  }
}

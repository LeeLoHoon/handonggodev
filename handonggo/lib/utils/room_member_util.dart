import 'package:handong_go/models/room_member_model.dart';

class RoomMemberUtil {
  static RoomMember _roomMember;

  static void setRoomMember(RoomMember roomMember) async {
    _roomMember = roomMember;
  }

  static RoomMember getRoomMember() {
    return _roomMember;
  }
}

import 'dart:convert';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:http/http.dart' as http;

class Translate {
  final String language;
  final String contents;
  final String clientId = "a2aLvejPuIqfuDiDQu71";
  final String clientSecret = "AXbAhFEJAD";

  Translate(this.language, this.contents);

  Future<String> translateKoToEn() async {
    final response = await http.post(
      'https://openapi.naver.com/v1/papago/n2mt',
      body: {"source": language, "target": "en", "text": contents},
      headers: {
        "X-Naver-Client-Id": clientId,
        "X-Naver-Client-Secret": clientSecret,
        "Content-Type": "application/x-www-form-urlencoded; charset=UTF-8"
      },
    );

    String enTitle = "";

    DecodeFirstResult firstResult =
        DecodeFirstResult.fromJSON(json.decode(response.body));

    DecodeSecondResult secondResult =
        DecodeSecondResult.fromJSON(firstResult.mes);
    DecodeLastResult lastResult = DecodeLastResult.fromJSON(secondResult.mes);
    enTitle = lastResult.translated;
    Duration(milliseconds: 1000);

    return enTitle;
  }

  Future<String> translateKoToVi() async {
    final response = await http.post(
      'https://openapi.naver.com/v1/papago/n2mt',
      body: {"source": language, "target": "vi", "text": contents},
      headers: {
        "X-Naver-Client-Id": clientId,
        "X-Naver-Client-Secret": clientSecret,
        "Content-Type": "application/x-www-form-urlencoded; charset=UTF-8"
      },
    );

    String viTitle = "";

    DecodeFirstResult firstResult =
        DecodeFirstResult.fromJSON(json.decode(response.body));

    DecodeSecondResult secondResult =
        DecodeSecondResult.fromJSON(firstResult.mes);
    DecodeLastResult lastResult = DecodeLastResult.fromJSON(secondResult.mes);
    viTitle = lastResult.translated;

    Duration(milliseconds: 1000);

    return viTitle;
  }

  Future<String> translateEnToKo() async {
    final response = await http.post(
      'https://openapi.naver.com/v1/papago/n2mt',
      body: {"source": language, "target": "ko", "text": contents},
      headers: {
        "X-Naver-Client-Id": clientId,
        "X-Naver-Client-Secret": clientSecret,
        "Content-Type": "application/x-www-form-urlencoded; charset=UTF-8"
      },
    );

    String koTitle = "";

    DecodeFirstResult firstResult =
        DecodeFirstResult.fromJSON(json.decode(response.body));

    DecodeSecondResult secondResult =
        DecodeSecondResult.fromJSON(firstResult.mes);
    DecodeLastResult lastResult = DecodeLastResult.fromJSON(secondResult.mes);
    koTitle = lastResult.translated;

    Duration(milliseconds: 1000);

    return koTitle;
  }

  Future<String> translateViToKo() async {
    final response = await http.post(
      'https://openapi.naver.com/v1/papago/n2mt',
      body: {"source": language, "target": "ko", "text": contents},
      headers: {
        "X-Naver-Client-Id": clientId,
        "X-Naver-Client-Secret": clientSecret,
        "Content-Type": "application/x-www-form-urlencoded; charset=UTF-8"
      },
    );

    String koTitle = "";

    DecodeFirstResult firstResult =
        DecodeFirstResult.fromJSON(json.decode(response.body));

    DecodeSecondResult secondResult =
        DecodeSecondResult.fromJSON(firstResult.mes);
    DecodeLastResult lastResult = DecodeLastResult.fromJSON(secondResult.mes);
    koTitle = lastResult.translated;
    print(koTitle);

    Duration(milliseconds: 1000);
    return koTitle;
  }
}

class Sentence {
  final String sentence;

  final DocumentReference reference;

  Sentence.fromMap(Map<String, dynamic> map, {this.reference})
      : sentence = map['testString'];

  Sentence.fromSnapshot(DocumentSnapshot snapshot)
      : this.fromMap(snapshot.data, reference: snapshot.reference);
}

class DecodeFirstResult {
  final Map<String, dynamic> mes;

  DecodeFirstResult({
    this.mes,
  });

  factory DecodeFirstResult.fromJSON(Map<String, dynamic> json) {
    return DecodeFirstResult(mes: json['message']);
  }
}

class DecodeSecondResult {
  final Map<String, dynamic> mes;

  DecodeSecondResult({
    this.mes,
  });

  factory DecodeSecondResult.fromJSON(Map<String, dynamic> json) {
    return DecodeSecondResult(mes: json['result']);
  }
}

class DecodeLastResult {
  final String srcLang;
  final String tarLang;
  final String translated;

  DecodeLastResult({
    this.srcLang,
    this.tarLang,
    this.translated,
  });

  factory DecodeLastResult.fromJSON(Map<String, dynamic> json) {
    return DecodeLastResult(
      srcLang: json['srcLangType'],
      tarLang: json['tarLangType'],
      translated: json['translatedText'],
    );
  }
}

class LocaleUtil {
  static String _locale;

  static void setLocale(String locale) async {
    _locale = locale;
  }

  static String getLocale() {
    return _locale;
  }

  static void setNull() {
    _locale = null;
  }
}

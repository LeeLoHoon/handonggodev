class Validators {
  static final RegExp _emailRegExp = RegExp(
    r'^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$',
  );
  static final RegExp _passwordRegExp = RegExp(
    r'^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$',
  );

  static isValidEmail(String email) {
    return _emailRegExp.hasMatch(email);
  }

  static isValidPassword(String password) {
    return _passwordRegExp.hasMatch(password);
  }

  static isValidAge(String age) {
    if (age == null) {
      return false;
    }
    return int.tryParse(age) != null;
  }

  static isValidName(String name) {
    return name.length >= 1 ? true : false;
  }

  static isValidNickName(String name) {
    return name.length >= 1 ? true : false;
  }

  static isValidProfile(String path) {
    return path.length >= 1 ? true : false;
  }

  static isLocationValid(double lat, double lng) {
    bool a;
    if (lat != null && lng != null)
      a = true;
    else
      a = false;
    return a;
  }

  static isValidList(List item) {
    return item.length >= 1 ? true : false;
  }

  static isValidCurrency(int index) {
    return index >= 0 ? true : false;
  }
}

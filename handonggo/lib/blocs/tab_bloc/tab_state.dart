import 'package:meta/meta.dart';

@immutable
class TabState {
  final int index;

  TabState({@required this.index});

  factory TabState.empty() {
    return TabState(index: 0);
  }

  TabState update({int index}) {
    return copyWith(index: index);
  }

  TabState copyWith({int index}) {
    return TabState(index: index ?? this.index);
  }

  @override
  String toString() {
    return '''TabState{
      index: $index}
    }''';
  }
}

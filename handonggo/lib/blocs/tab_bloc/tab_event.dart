import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class TabEvent extends Equatable {
  TabEvent([List props = const []]) : super(props);
}

class TabChange extends TabEvent {
  final int index;

  TabChange({@required this.index}) : super([index]);

  @override
  String toString() => 'TabChanged { TabChanged :$index }';
}

import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:handong_go/blocs/tab_bloc/bloc.dart';
import 'package:xml2json/xml2json.dart';

class TabBloc extends Bloc<TabEvent, TabState> {
  final Xml2Json xmlParser = Xml2Json();

  @override
  TabState get initialState => TabState.empty();

  @override
  Stream<TabState> mapEventToState(TabEvent event) async* {
    if (event is TabChange) {
      yield* _mapTabChangeToState(event.index);
    }
  }

  Stream<TabState> _mapTabChangeToState(int i) async* {
    yield state.update(index: i);
  }
}

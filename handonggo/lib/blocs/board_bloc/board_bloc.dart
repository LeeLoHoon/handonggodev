import 'dart:async';
import 'package:bloc/bloc.dart';
import 'bloc.dart';

class BoardBloc
    extends Bloc<BoardEvent, BoardState> {
  @override
  BoardState get initialState => BoardState.empty();

  @override
  Stream<BoardState> mapEventToState(
      BoardEvent event,
      ) async* {
    if (event is IsFreeCategorySelected) {
      yield* _mapIsFreeCategorySelecteToState();
    } else if (event is IsFoodCategorySelected) {
      yield* _mapIsFoodCategorySelectedToState();
    } else if (event is IsGroupCategorySelected) {
      yield* _mapIsGroupCategorySelectedToState();
    } else if (event is IsMarketCategorySelected) {
      yield* _mapIsMarketCategorySelectedToState();
    }
  }

  Stream<BoardState> _mapIsFreeCategorySelecteToState() async* {
    yield state.update(isFoodCategorySelected: false, isFreeCategorySelected: true, isGroupCategorySelected: false, isMarketCategorySelected: false);
  }

  Stream<BoardState> _mapIsFoodCategorySelectedToState() async* {
    yield state.update(isFoodCategorySelected: true, isFreeCategorySelected: false, isGroupCategorySelected: false, isMarketCategorySelected: false);
  }

  Stream<BoardState> _mapIsGroupCategorySelectedToState() async* {
    yield state.update(isFoodCategorySelected: false, isFreeCategorySelected: false, isGroupCategorySelected: true, isMarketCategorySelected: false);
  }

  Stream<BoardState> _mapIsMarketCategorySelectedToState() async* {
    yield state.update(isFoodCategorySelected: false, isFreeCategorySelected: false, isGroupCategorySelected: false, isMarketCategorySelected: true);
  }

}

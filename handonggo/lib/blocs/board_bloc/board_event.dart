import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class BoardEvent extends Equatable {
  BoardEvent([List props = const []]) : super(props);
}

class IsFreeCategorySelected extends BoardEvent {
  @override
  String toString() => 'IsFreeCategorySelected';
}

class IsFoodCategorySelected extends BoardEvent {
  @override
  String toString() => 'IsFoodCategorySelected';
}

class IsGroupCategorySelected extends BoardEvent {
  @override
  String toString() => 'IsGroupCategorySelected';
}

class IsMarketCategorySelected extends BoardEvent {
  @override
  String toString() => 'IsMarketCategorySelected';
}

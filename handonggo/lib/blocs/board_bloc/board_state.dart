import 'package:meta/meta.dart';

@immutable
class BoardState {
  final bool isFreeCategorySelected;
  final bool isFoodCategorySelected;
  final bool isGroupCategorySelected;
  final bool isMarketCategorySelected;

  BoardState(
      {@required this.isFreeCategorySelected,
      @required this.isFoodCategorySelected,
      @required this.isGroupCategorySelected,
      @required this.isMarketCategorySelected});

  factory BoardState.empty() {
    return BoardState(
        isFreeCategorySelected: true,
        isFoodCategorySelected: false,
        isGroupCategorySelected: false,
        isMarketCategorySelected: false);
  }

  BoardState update(
      {bool isFreeCategorySelected,
      bool isFoodCategorySelected,
      bool isGroupCategorySelected,
      bool isMarketCategorySelected}) {
    return copyWith(
      isFreeCategorySelected: isFreeCategorySelected,
      isFoodCategorySelected: isFoodCategorySelected,
      isGroupCategorySelected: isGroupCategorySelected,
      isMarketCategorySelected: isMarketCategorySelected,
    );
  }

  BoardState copyWith(
      {bool isFreeCategorySelected,
      bool isFoodCategorySelected,
      bool isGroupCategorySelected,
      bool isMarketCategorySelected}) {
    return BoardState(
        isFreeCategorySelected:
            isFreeCategorySelected ?? this.isFreeCategorySelected,
        isFoodCategorySelected:
            isFoodCategorySelected ?? this.isFoodCategorySelected,
        isGroupCategorySelected:
            isGroupCategorySelected ?? this.isGroupCategorySelected,
        isMarketCategorySelected:
            isMarketCategorySelected ?? this.isMarketCategorySelected);
  }

  @override
  String toString() {
    return '''BoardState {
      isFreeCategorySelected: $isFreeCategorySelected
      isFoodCategorySelected: $isFoodCategorySelected,
      isGroupCategorySelected: $isGroupCategorySelected,
      isMarketCategorySelected: $isMarketCategorySelected,
    }''';
  }
}

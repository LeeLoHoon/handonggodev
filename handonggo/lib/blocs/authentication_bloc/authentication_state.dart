import 'package:meta/meta.dart';

@immutable
class AuthenticationState {
  final bool uninitialized;
  final bool authenticated;
  final bool unAuthenticated;

  AuthenticationState(
      {@required this.uninitialized,
      @required this.authenticated,
      @required this.unAuthenticated});

  factory AuthenticationState.empty() {
    return AuthenticationState(
        uninitialized: false, authenticated: false, unAuthenticated: false);
  }

  AuthenticationState update(
      {bool uninitialized, bool authenticated, bool unAuthenticated}) {
    return copyWith(
        uninitialized: uninitialized,
        authenticated: authenticated,
        unAuthenticated: unAuthenticated);
  }

  AuthenticationState copyWith(
      {bool uninitialized, bool authenticated, bool unAuthenticated}) {
    return AuthenticationState(
        uninitialized: uninitialized ?? this.uninitialized,
        authenticated: authenticated ?? this.authenticated,
        unAuthenticated: unAuthenticated ?? this.unAuthenticated);
  }

  @override
  String toString() {
    return '''AuthenticationState {
      uninitialized: $uninitialized
      authenticated: $authenticated,
      unAuthenticated: $unAuthenticated
    }''';
  }
}

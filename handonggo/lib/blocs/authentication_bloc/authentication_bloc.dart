import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:handong_go/blocs/authentication_bloc/bloc.dart';
import 'package:handong_go/models/user_model.dart';
import 'package:handong_go/utils/user_repository.dart';
import 'package:handong_go/utils/user_util.dart';
import 'package:meta/meta.dart';

class AuthenticationBloc
    extends Bloc<AuthenticationEvent, AuthenticationState> {
  final UserRepository _userRepository;

  AuthenticationBloc({@required UserRepository userRepository})
      : assert(userRepository != null),
        _userRepository = userRepository;

  @override
  AuthenticationState get initialState => AuthenticationState.empty();

  @override
  Stream<AuthenticationState> mapEventToState(
    AuthenticationEvent event,
  ) async* {
    if (event is AppStarted) {
      yield* _mapAppStartedToState();
    } else if (event is LoggedIn) {
      yield* _mapLoggedInToState();
    } else if (event is LoggedOut) {
      yield* _mapLoggedOutToState();
    }
  }

  Stream<AuthenticationState> _mapAppStartedToState() async* {
    await Future.delayed(new Duration(seconds: 2));

    try {

      final isSignedIn = await _userRepository.isSignedIn();


      if (isSignedIn) {
        yield* _mapLoggedInToState();
      } else {
        yield state.update(authenticated: false, unAuthenticated: true);
      }
    } catch (_) {
      yield state.update(authenticated: false, unAuthenticated: true);
    }
  }

  Stream<AuthenticationState> _mapLoggedInToState() async* {
    try {
      UserUtil.setUser(User.fromDs(await Firestore.instance
          .collection('User')
          .document((await UserRepository().getUser()).uid)
          .get()));

      yield state.update(authenticated: true, unAuthenticated: false);
    } catch (e) {
      print('Logged In error: $e');
      yield state.update(authenticated: false, unAuthenticated: true);
    }
  }

  Stream<AuthenticationState> _mapLoggedOutToState() async* {
    try {
      await _userRepository.signOut();
      yield state.update(authenticated: false, unAuthenticated:  true);
    } catch (e) {
      print(e.toString());
    }
  }

}

import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class HomeEvent extends Equatable {
  HomeEvent([List props = const []]) : super(props);
}

class PageLoaded extends HomeEvent {
  @override
  String toString() {
    return 'PageLoaded==';
  }
}

class SearchChanged extends HomeEvent {
  final String search;

  SearchChanged({@required this.search}) : super([search]);
  @override
  String toString() {
    return 'SearchChanged : $search';
  }
}

class FABPressed extends HomeEvent {
  @override
  String toString() {
    return 'FABPressed';
  }
}


class AddScheduleBtnPressed extends HomeEvent {
  @override
  String toString() => "add schedule btn pressed";
}

class AddCourseBtnPressed extends HomeEvent {
  @override
  String toString() => "add Course btn Pressed ";
}

class CourseListUpdate extends HomeEvent {
  @override
  String toString() => "course list updated";
}

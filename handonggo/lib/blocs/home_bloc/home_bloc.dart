import 'dart:async';
import 'package:bloc/bloc.dart';
import 'bloc.dart';

class HomeBloc extends Bloc<HomeEvent, HomeState> {
  @override
  HomeState get initialState => HomeState.empty();
  String output = '';
  //GoogleTranslator _translator = new GoogleTranslator();

  @override
  Stream<HomeState> mapEventToState(HomeEvent event) async* {
    if (event is FABPressed) {
      yield* _mapFABPressedToState();
    } else if (event is AddCourseBtnPressed) {
      yield* _mapAddCoursePressedToState();
    }else if (event is AddScheduleBtnPressed) {
      yield* _mapAddScheduleBtnToState();
    }
  }

  Stream<HomeState> _mapFABPressedToState() async* {
    yield state.update(FAB: true);
  }

  Stream<HomeState> _mapAddCoursePressedToState() async* {
    yield state.update(addCoursePressed: true);
  }

  Stream<HomeState> _mapAddScheduleBtnToState() async* {
    yield state.update(FAB: true);
  }
}

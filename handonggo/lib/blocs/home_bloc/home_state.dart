
import 'package:meta/meta.dart';

@immutable
class HomeState {
  final List<String> bannerImg;
  final List<String> searchRanking;
  final List<String> itemList;

  final bool FAB;
  final bool addCoursePressed;

  HomeState(
      {@required this.bannerImg,
      @required this.searchRanking,
      @required this.itemList,

      @required this.FAB,
      @required this.addCoursePressed});

  factory HomeState.empty() {
    return HomeState(
      bannerImg: [],
      searchRanking: [],
      itemList: [],

      FAB: false,
      addCoursePressed: false,
    );
  }

  HomeState update({
    List<String> bannerImg,
    List<String> searchRanking,
    List<String> itemList,

    bool FAB,
    bool addCoursePressed,
  }) {
    return copyWith(
      bannerImg: bannerImg,
      searchRanking: searchRanking,
      itemList: itemList,

      FAB: FAB,
      addCoursePressed: addCoursePressed,
    );
  }

  HomeState copyWith({
    List<String> bannerImg,
    List<String> searchRanking,
    List<String> itemList,

    bool FAB,
    bool addCoursePressed,
  }) {
    return HomeState(
      bannerImg: bannerImg ?? this.bannerImg,
      searchRanking: searchRanking ?? this.searchRanking,
      itemList: itemList ?? this.itemList,

      FAB: FAB ?? false,
      addCoursePressed: addCoursePressed ?? false,
    );
  }
}

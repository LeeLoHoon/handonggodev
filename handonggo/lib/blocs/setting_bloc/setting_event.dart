import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class SettingEvent extends Equatable {
  SettingEvent([List props = const []]) : super(props);
}

class UserInfoEditPressed extends SettingEvent {
  @override
  String toString() =>
      'UserInformation Edit Button Pressed { userInfoEditPressed : true }';
}

class LogOutBtnPressed extends SettingEvent {
  @override
  String toString() => 'Logout button Pressed { logOutBtnPressed: true }';
}

class CancelLogout extends SettingEvent {
  @override
  String toString() =>
      'CancelLogout button Pressed { logOutBtnPressed: false }';
}

class DoLogout extends SettingEvent {
  @override
  String toString() => 'DoLogout button Pressed { doLogout: true }';
}

class LanguageBtnPressed extends SettingEvent {
  @override
  String toString() => 'Language button Pressed { LanguageBtnPressed: true }';
}



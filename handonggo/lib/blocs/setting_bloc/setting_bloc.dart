import 'dart:async';

import 'package:bloc/bloc.dart';

import 'bloc.dart';


class SettingBloc extends Bloc<SettingEvent, SettingState> {
  @override
  SettingState get initialState => SettingState.empty();

  @override
  Stream<SettingState> mapEventToState(SettingEvent event) async* {
    if (event is UserInfoEditPressed) {
      yield* _mapUserInfoEditPressedToState();
    } else if (event is LogOutBtnPressed) {
      yield* _mapLogOutBtnPressedToState();
    } else if (event is DoLogout) {
      yield* _mapDoLogoutToState();
    } else if (event is CancelLogout) {
      yield* _mapCancelLogoutToState();
    } else if (event is LanguageBtnPressed) {
      yield* _mapLanguageBtnPressedToState();
    }
  }

  Stream<SettingState> _mapUserInfoEditPressedToState() async* {
    yield state.update(
      userInfoEditPressed: true,
    );
  }

  Stream<SettingState> _mapLogOutBtnPressedToState() async* {
    yield state.update(
      logOutBtnPressed: true,
    );
  }

  Stream<SettingState> _mapCancelLogoutToState() async* {
    yield state.update(
      logOutBtnPressed: false,
    );
  }

  Stream<SettingState> _mapDoLogoutToState() async* {
    yield state.update(
      doLogoutPressed: true,
    );
  }

  Stream<SettingState> _mapLanguageBtnPressedToState() async* {
    yield state.update(
      languageBtnPressed: true,
    );
  }


}

import 'package:meta/meta.dart';

@immutable
class SettingState {
  final bool userInfoEditPressed;
  final bool logOutBtnPressed;
  final bool doLogoutPressed;
  final bool languageBtnPressed;


  SettingState({
    @required this.userInfoEditPressed,
    @required this.logOutBtnPressed,
    @required this.doLogoutPressed,
    @required this.languageBtnPressed,
  });

  factory SettingState.empty() {
    return SettingState(
      userInfoEditPressed: false,
      logOutBtnPressed: false,
      doLogoutPressed: false,
        languageBtnPressed: false
    );
  }

  SettingState update({
    bool userInfoEditPressed,
    bool logOutBtnPressed,
    bool doLogoutPressed,
    bool languageBtnPressed,
  }) {
    return copyWith(
      userInfoEditPressed: userInfoEditPressed,
      logOutBtnPressed: logOutBtnPressed,
      doLogoutPressed: doLogoutPressed,
        languageBtnPressed: languageBtnPressed,
    );
  }

  SettingState copyWith({
    bool userInfoEditPressed,
    bool logOutBtnPressed,
    bool doLogoutPressed,
    bool languageBtnPressed,
  }) {
    return SettingState(
        userInfoEditPressed: userInfoEditPressed ?? false,
        logOutBtnPressed: logOutBtnPressed ?? false,
        doLogoutPressed: doLogoutPressed ?? false,
        languageBtnPressed: languageBtnPressed ?? false,
        );
  }

  @override
  String toString() {
    return ''' 
      UserInformation Edit Button is $userInfoEditPressed,
      LogOut Button Pressed is $logOutBtnPressed,
      doLogoutPressed : $doLogoutPressed,
      languageBtnPressed: $languageBtnPressed,
    ''';
  }
}
